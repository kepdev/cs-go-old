var request = require('request');
var fs = require('fs');
var assert = require('assert');
var config = require('../config/config');

assert(config.MIN_ITEM_VALUE);

var prices = {};

function refreshPrices(callback) {
  request({url: 'http://api.steamanalyst.com/apiV2.php?key=' + config.STEAMANALYST_KEY, json: true}, function(err, res, body) {
    if (err || !body.hasOwnProperty('results')) {
      if (callback) callback(err || body);
      return;
    }

    var data = body.results;

    for (var i = 0; i < data.length; i++) {
      prices[data[i].market_name] = data[i];
    }

    if (callback) callback();
  });
}

setInterval(refreshPrices, 30 * 60 * 1000); // refresh every 30 minutes
refreshPrices();

getRawPrice = module.exports.getRawPrice = function(item_name, preferred) {
  if (!prices.hasOwnProperty(item_name)) return 0;
  var item = prices[item_name];
  if (item.hasOwnProperty('suggested_amount_' + (preferred || 'avg') + '_raw')) return item['suggested_amount_' + (preferred || 'avg') + '_raw'];
  if (item.hasOwnProperty('safe_price_raw')) return item.safe_price_raw;
  if (item.ongoing_price_manipulation == '1') return 0;

  return item.avg_price_7_days_raw;
}

getPrice = module.exports.getPrice = function(item_name, preferred) {
  var raw = getRawPrice(item_name, preferred);
  if (isNaN(raw)) return 0;
  if (/(Case|eSports) Key/.test(item_name)) return Math.round(raw * 0.90 * 100) * 1000;
  if (raw < 5) return Math.round(raw * 0.75 * 100) * 1000;
  if (raw < 10) return Math.round(raw * 0.875 * 100) * 1000;
  if (raw < 20) return Math.round(raw * 0.95 * 100) * 1000;
  return Math.floor(raw * 100) * 1000;
}

getIcon = module.exports.getIcon = function(item_name) {
  if (!prices.hasOwnProperty(item_name)) return '';
  return prices[item_name].img.replace('http://', '//');
}

getJunk = module.exports.getJunk = function(item_name) {
  if (getPrice(item_name) < config.MIN_ITEM_VALUE * 100) return "Junk"; // less than $x items
  if (item_name.indexOf('Souvenir') === 0) return "Souvenir";  // souvenirs BUT NOT SOUVENIR PACKAGES
  if (item_name.indexOf('Music Kit') !== -1) return "Music Kit"; // music kits
  if (item_name.indexOf('Swap Tool') !== -1) return "Junk";    // swap tool
  if (item_name.indexOf('Name Tag') !== -1) return "Junk";     // name tags
  if (item_name.indexOf('Capsule') !== -1) return "Junk";      // sticker capsules
  if (item_name.indexOf('Legends') !== -1) return "Junk";      // sticker capsules
  if (item_name.indexOf('Challengers') !== -1) return "Junk";  // sticker capsules
  if (item_name.indexOf('Sticker') !== -1) return "Sticker";   // stickers
  if (item_name.indexOf('Gift Package') !== -1) return "Junk"; // gift packages
  if (item_name.indexOf('Parcel') !== -1) return "Junk";       // gift packages v2
  if (item_name.indexOf('Pallet') !== -1) return "Junk";       // gift packages v3
  if (item_name.indexOf('Access Pass') !== -1) return "Access Pass"; // access passes
  if (item_name.indexOf('★ StatTrak™ Falchion Knife | Crimson Web (Battle-Scarred)') !== -1)	return "Price fix";  //Price fixed item
  if (/Case(?:\s\d+)?$/.test(item_name)) return "Junk";         // weapon cases
}
