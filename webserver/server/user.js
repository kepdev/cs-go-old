var assert = require('better-assert');
var async = require('async');
var request = require('request');
var timeago = require('timeago');
var crypto = require('crypto');
var lib = require('./lib');
var database = require('./database');
var sendEmail = require('./sendEmail');
var uuid = require('uuid');
var _ = require('lodash');
var config = require('../config/config');
var SteamTrades = require('steamtrades');
var itemInfo = require('./itemInfo');
var lib = require('./lib');

var sessionOptions = {
    httpOnly: true,
    secure: config.PRODUCTION
};

var steamTrades = new SteamTrades({
    access_token: config.STEAMTRADES_API_KEY
});

/**
 * GET
 * Public API
 * Verifies user and creates a session
 */
exports.steamLogin = function(req, res, next) {
    var user = req.user;
console.log(req.user ? req.user.steamid : req.user);
    assert(user && user.steamid);

    // is the user registered? try to register him first
    database.registerOrLogin(user.steamid, user.username, user.avatar.large, function(err, sessionId) {
        if (err) {
            return next(new Error('Unable to register user: \n' + err));
        }

        // phpsessid is here only to fool people :^)
        res.redirect(config.SITE_URL + '/login/redirect?PHPSESSID=' + crypto.randomBytes(128).toString('hex') + '&userid=' + sessionId);
    });
}

/**
 * GET
 * Public API
 * Logs in using the sid
 */

exports.loginRedirect = function(req, res, next) {
	console.log(String(req.query.userid));
    var sid = String(req.query.userid);
    if (!sid || sid == "") {
        return next(new Error('No session ID'));
    }

    res.cookie('id', sid, sessionOptions);
    res.redirect('/');
}

/**
 * POST
 * Logged API
 * Logout the current user
 */
exports.logout = function(req, res, next) {
    var sessionId = req.cookies.id;
    var userId = req.user.id;

    assert(sessionId && userId);

    database.expireSessionsByUserId(userId, function(err) {
        if (err)
            return next(new Error('Unable to logout got error: \n' + err));
        res.redirect('/');
    });
};

/**
 * GET
 * Logged API
 * Shows the graph of the user profit and games
 */
exports.profile = function(req, res, next) {

    var user = req.user; //If logged here is the user info
    var steamid = lib.removeNullsAndTrim(req.params.steamid);

    var page = null;
    if (req.query.p) { //The page requested or last
        page = parseInt(req.query.p);
        if (!Number.isFinite(page) || page < 0)
            return next('Invalid page');
    }

    if (!steamid)
        return next('No steamid in profile');

    database.getPublicStats(steamid, function(err, stats) {
        if (err) {
            if (err === 'USER_DOES_NOT_EXIST')
               return next('User does not exist');
            else
                return next(new Error('Cant get public stats: \n' + err));
        }

        /**
         * Pagination
         * If the page number is undefined it shows the last page
         * If the page number is given it shows that page
         * It starts counting from zero
         */

        var resultsPerPage = 50;
        var pages = Math.floor(stats.games_played / resultsPerPage);

        if (page && page >= pages)
            return next('User does not have page ', page);

        // first page absorbs all overflow
        var firstPageResultCount = stats.games_played - ((pages-1) * resultsPerPage);

        var showing = page ? resultsPerPage : firstPageResultCount;
        var offset = page ? (firstPageResultCount + ((pages - page - 1) * resultsPerPage)) : 0 ;

        if (offset > 100000) {
          return next('Sorry we can\'t show games that far back :( ');
        }

        var tasks = [
            function(callback) {
                database.getUserNetProfitLast(stats.user_id, showing + offset, callback);
            },
            function(callback) {
                database.getUserPlays(stats.user_id, showing, offset, callback);
            }
        ];


        async.parallel(tasks, function(err, results) {
            if (err) return next(new Error('Error getting user profit: \n' + err));

            var lastProfit = results[0];

            var netProfitOffset = stats.net_profit - lastProfit;
            var plays = results[1];


            if (!lib.isInt(netProfitOffset))
                return next(new Error('Internal profit calc error: ' + steamid + ' does not have an integer net profit offset'));

            assert(plays);

            plays.forEach(function(play) {
                play.timeago = timeago(play.created);
            });

            var previousPage;
            if (pages > 1) {
                if (page && page >= 2)
                    previousPage = '?p=' + (page - 1);
                else if (!page)
                    previousPage = '?p=' + (pages - 1);
            }

            var nextPage;
            if (pages > 1) {
                if (page && page < (pages-1))
                    nextPage ='?p=' + (page + 1);
                else if (page && page == pages-1)
                    nextPage = stats.steamid;
            }

            res.render('user', {
                user: user,
                stats: stats,
                plays: plays,
                net_profit_offset: netProfitOffset,
                showing_last: !!page,
                previous_page: previousPage,
                next_page: nextPage,
                games_from: stats.games_played-(offset + showing - 1),
                games_to: stats.games_played-offset,
                pages: {
                    current: page == 0 ? 1 : page + 1 ,
                    total: Math.ceil(stats.games_played / 100)
                }
            });
        });

    });
};

/**
 * GET
 * Shows the request bits page
 * Restricted API to logged users
 **/
exports.request = function(req, res) {
    var user = req.user; //Login var
    assert(user);

    res.render('request', { user: user });
};

/**
 * GET
 * Restricted API
 * Shows the account page, the default account page.
 **/
exports.account = function(req, res, next) {
    var user = req.user;
    assert(user);

    var tasks = [
        function(callback) {
            database.getDepositsAmount(user.id, callback);
        },
        function(callback) {
            database.getWithdrawalsAmount(user.id, callback);
        },
        function(callback) {
            database.getUserNetProfit(user.id, callback);
        }
    ];

    async.parallel(tasks, function(err, ret) {
        if (err)
            return next(new Error('Unable to get account info: \n' + err));

        var deposits = ret[0];
        var withdrawals = ret[1];
        var net = ret[2];
        user.deposits = !deposits.sum ? 0 : deposits.sum;
        user.withdrawals = !withdrawals.sum ? 0 : withdrawals.sum;
        user.net_profit = net.profit;

        res.render('account', { user: user, csrfToken: req.csrfToken() });
    });
};

/**
 * POST
 * Restricted API
 * Changes user's trade link
 */
exports.setTradeLink = function(req, res, next) {
    var user = req.user;
    assert(user);

    var link = lib.removeNullsAndTrim(req.body.trade_link);
    if (!lib.isValidTradeLink(link, user.steamid)) {
        res.flash('error', 'Your trade link is incorrect.');
        return res.redirect('/account');
    }

    database.setTradeLink(user.id, link, function(err) {
        if (err) {
            return next(new Error('Unable to set tradelink: \n' + err));
        }

        res.flash('advice', 'Trade link set.');
        res.redirect('/account');
    });
}

// that's shitty i know
var inventory_callbacks = {};
var trade_callbacks = {};
exports.webhook = function(req, res, next) {
    var data = req.body;

    switch (req.query.event) {
        case 'inventory_scan':
            if (inventory_callbacks.hasOwnProperty(data.steam_id))
                inventory_callbacks[data.steam_id](data);

            break;
        case 'trade_status_updated':
            if (trade_callbacks.hasOwnProperty(data.id))
                trade_callbacks[data.id](data);

            break;
    }

    res.end();
}

function getInventory(user, force_refresh, callback) {
    // check for trade url first :^)
    if (!user.trade_link) {
        return callback(new Error('Your trade link is not set.'), false, []);
    }

    // check in db if there's any inventory cached
    database.getInventory(user.id, function (err, result) {
        if (err) return callback(new Error('Database error.'), false, []);

        function refresh(old_items) {
            // request inventory scans
            steamTrades.scanInventory(user.trade_link, 1, true, function (err, scan) {
                if (err) {
                    callback(new Error('Could not retrieve inventory.'), !!old_items, old_items || []);
                    return;
                }

                // this shiz will get called as soon as the webhook is called
                inventory_callbacks[user.steamid] = function (inventory) {
                    // get rid of the 'callback'
                    delete inventory_callbacks[user.steamid];

                    if (inventory.failed) {
                        var msg = 'Your trade link is incorrect or your inventory is not set to Public. Fix this and try again in 3 minutes.';
                        
                        // return empty inventory list, or the cached one if available
                        callback(new Error(msg), !!old_items, old_items || []);

                        // update the inventory in db (don't wait for the callback, fuck it)
                        database.updateInventory(user.id, msg, old_items || [], typeof old_items === 'undefined', function() {});

                        return;
                    }

                    // wow all succeeded
                    callback(null, false, inventory.items);

                    // store in db
                    database.updateInventory(user.id, msg, inventory.items, typeof old_items === 'undefined', function() {});
                }
            });
        }

        // if no entry was found in the db, force refresh
        if (!result) return refresh();
        // if used requested a refresh
        if (force_refresh) {
            // time since last refresh
            var time = Date.now() - result.retrieved;
            // refresh if cached inventory is older than 3 minutes, error otherwise
            if (time > 3 * 60 * 1000) {
                refresh(result.items);
            } else {
                callback(new Error('You can refresh inventory again in ' + (3 * 60 - time / 1000).toFixed() + ' seconds.'), result.retrieved, result.items || []);
            }

            return;
        }

        // otherwise we just get normal cached inventory
        callback(null, result.retrieved, result.items);

    });
}

/**
 * GET
 * Restricted API
 * Shows the deposit history
 **/
exports.deposit = function(req, res, next) {
    var user = req.user;
    assert(user);

    if (config.DISABLE_BOTS) {
        user.error = "Bots are down for maintenance.";
    }

    database.getDeposits(user.id, function(err, deposits) {
        if (err) {
            return next(new Error('Unable to load inventory: \n' + err.message));
        }

        res.render('deposit', { user: user, deposits: deposits });
    });
};

var locks = [];
exports.depositJson = function(req, res, next) {
    var user = req.user;
    assert(user);

    if (config.DISABLE_BOTS) 
        return res.json({error: 'Bots are down for maintenance.'});

    if (!user.trade_link)
        return res.json({error: 'Trade link not set.'});

    if (locks.indexOf(user.id) !== -1)
        return res.json({error: 'You can have only one pending deposit or withdrawal.'});

    locks.push(user.id);

    // no timeout
    req.socket.setKeepAlive(true);
    req.setTimeout(0);
    
    async.waterfall([
        function(callback) {
            // check for pending offers
            database.hasPendingOffers(user.id, function (err, result) {
                if (err) return callback(new Error('Database error'));
                if (result) return callback(new Error('You can have only one pending deposit or withdrawal.'));

                // next step - get inventory
                callback();
            })
        },
        function(callback) {
            // parse input

            // single item - put it in an array
            if (typeof req.body.items == 'string')
                req.body.items = [req.body.items];

            // not an array? error
            if (!Array.isArray(req.body.items))
                return callback(new Error('No items selected.'));

            // item limits
            var limit = user.admin ? 100 : 10;

            if (req.body.items.length > limit)
                return callback(new Error('A maximum of ' + limit + ' items can be deposited at once.'));

            var itemids = req.body.items;

            // now get user's inventory
            getInventory(user, !!req.query.refresh, function (err, retrieved, inventory) {
                if (err)
                    return callback(new Error('Could not get inventory.'));

                var items = [];
                var value = 0;

                // find items in user inventory (to check prices, etc.)
                for (var i = 0; i < inventory.length; i++) {
                    if (itemids.indexOf(inventory[i].id) === -1) continue;

                    var name = inventory[i].category.steam_market_hash;
                    var junk = itemInfo.getJunk(name);
                    var price = itemInfo.getPrice(name, 'min');

                    if (junk) 
                        return callback(new Error('Cannot deposit ' + name + ': item is junk.'));

                    value += price;
                    items.push(inventory[i]);

                    continue;
                }

                if (itemids.length !== items.length) 
                    return callback(new Error('Some of your items are no longer available (' + items.length + '/' + itemids.length + ')'));

                // next step - send trade offer
                callback(null, items, value);
            })
        },
        function(items, value, callback) {
            // generate a cool security code
            var code = lib.generateSecurityCode();

            // send the trade offer via steamtrad.es api
            steamTrades.requestItems(user.trade_link, _.pluck(items, 'id'), 'Deposit valued at ' + lib.formatSatoshis(value, 0) + ' coins. Security code: ' + code, function (err, offer) {
                // todo: nicer error handling (for unavailable/locked items, etc.)
                if (err)
                    return callback(new Error(err.message)); // todo: log error? no?

                // insert to the db
                // don't wait for the callback and just hope it succeeded :^)
                database.addDeposit(offer.trade_id, user.id, items, value, code, function (err) {
                    if (err) {
                        console.error(err);
                    }
                });

                // listen to webhooks
                trade_callbacks[offer.trade_id] = function (data) {
                    // delete the callback if we don't need it
                    if (data.status !== 'in_progress' && data.status !== 'pending')
                        delete trade_callbacks[offer.trade_id];

                    // trade offer failed :(
                    if (data.status == 'failed') 
                        return callback(new Error('Trade offer failed. Please retry.'));

                    if (data.status == 'denied') {
                        var error;
                        switch (data.denial_reason) {
                            case 'would_escrow':        error = 'You need to have your mobile confirmations enabled for at least a week.'; break;
                            case 'private_inventory':   error = 'Your inventory needs to be set to Public.'; break;
                            case 'must_not_trade':      error = 'You cannot trade with the bot.'; break;
                            case 'item_gone':           error = 'The items you selected are missing'; break;
                            case 'invalid_trade_url':   error = 'The trade URL you supplied is invalid.'; break;
                            case 'impossible_item_combination': error = 'You can only select 1 item of a kind.'; break;
                            default:                    error = 'Try again in a few minutes.';
                        }

                        return callback(new Error(error));
                    }

                    // trade offer sent :)
                    if (data.status == 'in_progress')
                        return callback(null, data.tradeoffer_url, code);
                }
            })
        }],
    function (err, url, code) {
        // remove lock
        locks.splice(locks.indexOf(user.id), 1);

        if (err) return res.json({error: err.message});

        // all went well, return json :^)
        res.json({
            code: code,
            url: url
        });
    });
}

exports.withdrawJson = function(req, res, next) {
    var user = req.user;
    assert(user);

    if (config.DISABLE_BOTS) 
        return res.json({error: 'Bots are down for maintenance.'});

    if (!user.trade_link)
        return res.json({error: 'Trade link not set.'});

    if (locks.indexOf(user.id) !== -1)
        return res.json({error: 'You can have only one pending deposit or withdrawal.'});
    
    locks.push(user.id);

    // no timeout
    req.socket.setKeepAlive(true);
    req.setTimeout(0);

    async.waterfall([
        function(callback) {
            // check for pending offers
            database.hasPendingOffers(user.id, function (err, result) {
                if (err) return callback(new Error('Database error'));
                if (result) return callback(new Error('You can have only one pending deposit or withdrawal.'));

                // next step - get items, count price, etc.
                callback();
            })
        },
        function(callback) {
            // parse input

            // single item - put it in an array
            if (typeof req.body.items == 'string')
                req.body.items = [req.body.items];

            // not an array? error
            if (!Array.isArray(req.body.items))
                return callback(new Error('No items selected.'));

            // item limits
            var limit = user.admin ? 100 : 10;

            if (req.body.items.length > limit)
                return callback(new Error('A maximum of ' + limit + ' items can be withdrawn at once.'));

            var itemids = req.body.items;

            // grab the items from the db
            database.getItems(itemids, function(err, items) {
                if (err)
                    return callback(new Error('Database error'));

                // weird items that shouldn't even be in the db
                if (items.length !== itemids.length)
                    return callback(new Error('Some of the items you selected are missing.'));

                var value = 0;
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    // items alraedy withdrawn or never deposited
                    if (item.withdrawal_id || !item.received)
                        return callback(new Error('Some of the items you selected are missing.'));

                    // now we count the price
                    var price = itemInfo.getPrice(item.name, 'avg');

                    // generally this shit doesn't happen.
                    if (!price || isNaN(price) || price < 100)
                        return callback(new Error('Some of the items you selected are missing.'));

                    value += price;
                }

                // can user afford it?
                if (value > user.balance)
                    return callback(new Error('You cannot afford that withdrawal.'));

                // again - doesn't happen.
                if (items.length < 1 || value < 100)
                    return callback('No items selected.');

                // next step - take balance
                callback(null, items, value);
            });
        },
        function(items, value, callback) {
            database.addBalance(user.id, -value, function(err) {
                if (err)
                    return callback(new Error('You cannot afford that withdrawal.'));

                // next step - make the offer :^)
                callback(null, items, value);
            });
        },
        function(items, value, callback) {
            // generate a cool security code
            var code = lib.generateSecurityCode();

            // send the trade offer via steamtrad.es api
            var itemids = _.pluck(items, 'id');
            steamTrades.sendItems(user.trade_link, itemids, 'Withdrawal valued at ' + lib.formatSatoshis(value, 0) + ' coins. Security code: ' + code, function (err, offer) {
                // todo: nicer error handling!
                if (err) {
                    // trade failed - give the balance back
                    database.addBalance(user.id, value, function(err) {});

                    // return the error
                    return callback(new Error(err.message));
                }
                
                // insert into the db
                database.addWithdrawal(offer.trade_id, user.id, itemids, value, code, function(err) {
                    if (err) {
                        console.error(err);
                    }
                });

                // listen to webhooks
                trade_callbacks[offer.trade_id] = function (data) {
                    // delete the callback if we don't need it
                    if (data.status !== 'in_progress' && data.status !== 'pending')
                        delete trade_callbacks[offer.trade_id];

                    // trade offer failed :(
                    if (data.status == 'failed') 
                        return callback(new Error('Trade offer failed. Please retry.'));

                    if (data.status == 'denied') {
                        var error;
                        switch (data.denial_reason) {
                            case 'would_escrow':        error = 'You need to have your mobile confirmations enabled for at least a week.'; break;
                            case 'private_inventory':   error = 'Your inventory needs to be set to Public.'; break;
                            case 'must_not_trade':      error = 'You cannot trade with the bot.'; break;
                            case 'item_gone':           error = 'The items you selected are missing'; break;
                            case 'invalid_trade_url':   error = 'The trade URL you supplied is invalid.'; break;
                            case 'impossible_item_combination': error = 'You can only select 1 item of a kind.'; break;
                            default:                    error = 'Try again in a few minutes.';
                        }

                        return callback(new Error(error));
                    }

                    // trade offer sent :)
                    if (data.status == 'in_progress')
                        return callback(null, data.tradeoffer_url, code);

                    // todo: remove this 'callback' when the trade is done

                    // other statuses are handled by the depositor
                }
            })
        }],
    function (err, url, code) {
        // remove lock
        locks.splice(locks.indexOf(user.id), 1);

        if (err) return res.json({error: err.message});

        // all went well
        res.json({
            code: code,
            url: url
        });
    });
}

 /**
  * GET
  * Restricted API
  * Shows the transfer history
  **/
exports.transfer = function(req, res, next) {
  var user = req.user;
  assert(user);

  database.getTransfers(user.id, function(err, transfers) {
      if (err)
          return next(new Error('Unable to get transfers: ' + err));

      res.render('transfer', { user: user, transfers: transfers });
  });
};

exports.transferJson = function(req, res, next) {
    var user = req.user;
    assert(user);


    database.getTransfers(user.id, function(err, transfers) {
        if (err)
            return next(new Error('Unable to get transfers: ' + err));

        res.json(transfers);
    });
};

  /**
   * GET
   * Restricted API
   * Shows the transfer request page
   **/

exports.transferRequest = function(req, res) {
    assert(req.user);
    res.render('transfer-request', { user: req.user, id: uuid.v4(), csrfToken: req.csrfToken() });
};


 /**
  * POST
  * Restricted API
  * Process a transfer (tip)
  **/

 exports.handleTransferRequest = function (req,res,next){
     var user = req.user;
     assert(user);
     var uid = req.body['transfer-id'];
     var amount = lib.removeNullsAndTrim(req.body.amount);
     var toSteamID = lib.removeNullsAndTrim(req.body['to-steamid']);
     var password = lib.removeNullsAndTrim(req.body.password);
     var otp = lib.removeNullsAndTrim(req.body.otp);
     var r =  /^[1-9]\d*(\.\d{0,2})?$/;
     if (!r.test(amount))
         return res.render('transfer-request', { user: user, id: uuid.v4(),  warning: 'Not a valid amount.' });

    amount = Math.round(parseFloat(amount) * 100);

    toSteamID = toSteamID.replace(/^.*(\d{17})\/?$/, '$1')

    if (amount < 1000)
        return res.render('transfer-request', { user: user, id: uuid.v4(),  warning: 'Must transfer at least 10 coins.' });

    if (user.steamid.toLowerCase() === toSteamID.toLowerCase()) 
        return res.render('transfer-request', { user: user,  id: uuid.v4(), warning: 'Can\'t send money to yourself.'});

    if (!user.verified)
        return res.render('transfer-request', { user: user,  id: uuid.v4(), warning: 'You need to make a deposit first to be able to transfer.'});

    // Check destination user

    database.makeTransfer(uid, user.id, toSteamID, amount, function (err) {
        if (err) {
            if (err === 'NOT_ENOUGH_BALANCE')
                return res.render('transfer-request', {user: user, id: uuid.v4(), warning: 'Not enough balance for transfer.'});
            if (err === 'USER_NOT_EXIST')
                return res.render('transfer-request', {user: user, id: uuid.v4(), warning: 'Could not find user.'});
            if (err === 'TRANSFER_ALREADY_MADE')
                return res.render('transfer-request', {user: user, id: uuid.v4(), warning: 'You already submitted this.'});

            console.error('[INTERNAL_ERROR] could not make transfer: ', err);
            return res.render('transfer-request', {user: user, id: uuid.v4(), warning: 'Could not make transfer.'});
        }

        res.flash('advice', 'Transfer has been made.');
        return res.redirect('/transfer');
    });

 };

/**
 * GET
 * Restricted API
 * Shows the withdrawal history
 **/

exports.withdraw = function(req, res, next) {
    var user = req.user;
    assert(user);

    if (config.DISABLE_BOTS) {
        user.error = "Bots are down for maintenance.";
    }

    database.getWithdrawals(user.id, function(err, withdrawals) {
        if (err)
            return next(new Error('Unable to get withdrawals or inventory: \n' + err));


        res.render('withdraw', { user: user, withdrawals: withdrawals });
    });
};


/**
 * GET
 * Restricted API
 * Returns a nice JSON list of items present in the bots
**/

exports.itemsJson = function(req, res, next) {
    var user = req.user;
    assert(user);
    
    database.getAvailableItems(function(err, inventory) {
        if (err) 
            return next(new Error('Error getting available items.'));

        // list of items
        // item name is the key
        // keys of the object are: price, icon, itemids
        var result = {};

        for (var i = 0; i < inventory.length; i++) {
            var item = inventory[i];

            if (!result.hasOwnProperty(item.name)) {
                var price = itemInfo.getPrice(item.name, 'avg');
                var icon = itemInfo.getIcon(item.name);

                result[item.name] = {
                    price: price / 100,
                    icon: icon,
                    itemids: []
                };
            }

            result[item.name].itemids.push(item.id);
        }

        res.json({
            items: result,
            csrfToken: req.csrfToken()
        });
    });

}

/**
 * GET
 * Restricted API
 * Returns a nice JSON list of items present in user's inventory
**/

exports.inventoryJson = function(req, res, next) {
    var user = req.user;
    assert(user);

    // no timeout
    req.socket.setKeepAlive(true);
    req.setTimeout(0);

    var refresh = !config.DISABLE_BOTS && !!req.query.refresh;

    getInventory(user, refresh, function (err, retrieved, items) {
        var result = {};
        if (err) {
            result.error = err.message;
        }
        result.retrieved = retrieved;
        result.retrieved_str = timeago(retrieved);
        result.refreshed = !retrieved;

        result.items = {};

        result.csrfToken = req.csrfToken();

        for (var i = 0; i < items.length; i++) {
            var item = items[i];

            if (!result.items.hasOwnProperty(item.category.steam_market_hash)) {
                var price = itemInfo.getPrice(item.category.steam_market_hash, 'min');
                var junk = itemInfo.getJunk(item.category.steam_market_hash);

                result.items[item.category.steam_market_hash] = {
                    price: junk ? 0 : price / 100,
                    icon: item.category.preview_url,
                    junk: junk,
                    itemids: []
                };
            }

            result.items[item.category.steam_market_hash].itemids.push(item.id);
        }

        res.json(result);
    });
}

/**
 * GET
 * Restricted API
 * Shows the support page
 **/
exports.contact = function(req, res, next) {
    assert(req.user);
    res.render('support', { user: req.user })
};

/**
 * GET
 * Restricted API
 * Shows referrals page
 **/

exports.referrals = function(req, res, next) {
    var user = req.user;
    assert(user);

    async.parallel({
        referred: function(callback) {
            database.getReferred(user.id, callback);
        },
        referrer: function(callback) {
            if (!user.referrer_id) return callback();
            database.getUserByID(user.referrer_id, callback);
        }
    }, function (err, results) {
        if (err) return next(new Error('Error creating referral page:' + err));

        var sum = _.reduce(results.referred, function (memo, ref) { return memo + ref.referral_profit; }, 0);
        var level = lib.getReferralLevel(user.referred);
        var profit = lib.getReferralProfit(user.referred);

        res.render('referral', { user: user, referred: results.referred, sum: sum, level: level, profit: profit, referrer: results.referrer, csrfToken: req.csrfToken() });
    });
}

exports.referralsGenerateCode = function(req, res, next) {
    var user = req.user;
    assert(user);

    var code = lib.generateReferralCode(user);
    // if wants custom code check if elegible
    if (req.body.hasOwnProperty('custom')) {
        if (['twitch', 'youtube', 'moderator', 'admin'].indexOf(user.userclass) === -1) {
            req.flash('error', 'You need to have your Twitch or YouTube account connected to be able to generate a custom code.');
            res.redirect('/referral');
            return;
        }

        code = String(req.body.custom);
    }

    // trim code, make it proper and shit
    code = code
        .replace(/[^A-Za-z0-9-_]/g, '')
        .substring(0, 12)
        .toUpperCase();

    // check if code present in the db, return error otherwise
    database.setReferralCode(user.id, code, function (err) {
        if (err) {
            if (err === 'CODE_TAKEN') {
                res.flash('error', 'Code already taken, try another one.');
                res.redirect('/referral');
                return;
            }

            return next(err);
        }

        res.flash('advice', 'Your new code is: ' + code);
        res.redirect('/referral');
    });
}

exports.referralsUseCode = function(req, res, next) {
    var user = req.user;
    assert(user);

    var code = String(req.body.code).toUpperCase();

    database.useReferralCode(user.id, user.verified, code, function (err, referrer, bonus) {
        if (err) {
            var error;
            if (err === 'CODE_ALREADY_USED') error = 'You already used a code.';
            if (err === 'INCORRECT_CODE') error = 'This code is incorrect.';
            if (err === 'OWN_CODE') error = 'You can\'t use your own code.'

            if (error) {
                res.flash('error', error);
                res.redirect('/referral');
                return;
            }

            return next(err);
        }

        res.flash('advice', 'You have used a code and have been credited ' + lib.formatSatoshis(bonus, 0) + ' coins.');
        res.redirect('/referral');
    });
}

exports.connections = function(req, res, next) {
    var user = req.user;
    assert(user);

    database.getConnections(user.id, function (err, data) {
        if (err) return next(new Error('Could not get connections: \n' + err));

        connections = {};
        data.forEach(function (conn) {
            connections[conn.service] = conn;
        });

        res.render('connections', { user: user, connections: connections });
    });
}

exports.twitchConnection = function(req, res, next) {
    var user = req.user;
    assert(user);
    var profile = req.connectionProfile;
    assert(profile);

    // do the perks here
    if (['admin', 'moderator'].indexOf(user.userclass) !== -1) {
        res.flash('error', 'You already have a rank.');
        res.redirect('/connections');
        return;
    }
    if (profile.followers < 10000 || profile.views < 100000) {
        res.flash('error', 'You don\'t meet the requirements of 10000 followers and 100000 views.');
        res.redirect('/connections');
        return;
    }

    database.addConnection(user.id, 'twitch', profile.name, null, profile, function (err) {
        if (err) {
            // but why?
            res.flash('error', 'Could not connect to Twitch.');
            res.redirect('/connections');
            return;
        }

        database.setUserClass(user.id, 'twitch', function(){});
        res.flash('advice', 'Connected via Twitch successfully.');
        res.redirect('/connections');
    });
}

exports.youtubeConnection = function(req, res, next) {
    var user = req.user;
    assert(user);
    var profile = req.connectionProfile;
    assert(profile);

    // do the perks here
    if (['admin', 'moderator'].indexOf(user.userclass) !== -1) {
        res.flash('error', 'You already have a rank.');
        res.redirect('/connections');
        return;
    }
    if (profile.followers < 10000 || profile.views < 100000) {
        res.flash('error', 'You don\'t meet the requirements of 10000 followers and 100000 views.');
        res.redirect('/connections');
        return;
    }

    database.addConnection(user.id, 'youtube', profile.name, null, profile, function (err) {
        if (err) {
            // but why?
            res.flash('error', 'Could not connect to Youtube.');
            res.redirect('/connections');
            return;
        }

        database.setUserClass(user.id, 'youtube', function(){});
        res.flash('advice', 'Connected via YouTube successfully.');
        res.redirect('/connections');
    });
}