var assert = require('better-assert');
var crypto = require('crypto');
var config = require('../config/config');
var SteamID = require('steamid');
var url = require('url');

exports.randomHex = function(bytes) {
    var buff;

    try {
        buff = crypto.randomBytes(bytes);
    } catch (ex) {
        console.log('Caught exception when trying to generate hex: ', ex);
        buff = crypto.pseudoRandomBytes(bytes);
    }

    return buff.toString('hex');
};

exports.sha = function(str) {
    var shasum = crypto.createHash('sha256');
    shasum.update(str);
    return shasum.digest('hex');
};

exports.isUUIDv4 = function(uuid) {
    return (typeof uuid === 'string') && uuid.match(/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i);
};

exports.isValidTradeLink = function(trade_link, steamid) {
    // check if the trade url is supplied and looks like a correct steam trade link
    var parsed_url = url.parse(trade_link, true);
    if (!trade_link || !parsed_url || !parsed_url.query.partner || !parsed_url.query.token || parsed_url.query.token.length !== 8 || isNaN(parseInt(parsed_url.query.partner))) {
        return false;
    }

    if (steamid) {
        var accountid = parseInt(parsed_url.query.partner);
        var sid;
        try {
            sid = new SteamID(steamid);
        } catch (e) {
            return false;
        }

        return sid.accountid == accountid;
    } else {
        return true;
    }
}

exports.formatSatoshis = function(n, decimals) {
    if (typeof decimals === 'undefined')
        decimals = 2;

    return Math.floor(n/100).toFixed(decimals).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};

exports.isInt = function isInteger (nVal) {
    return typeof nVal === "number" && isFinite(nVal) && nVal > -9007199254740992 && nVal < 9007199254740992 && Math.floor(nVal) === nVal;
};

exports.hasOwnProperty = function(obj, propName) {
    return Object.prototype.hasOwnProperty.call(obj, propName);
};

exports.getOwnProperty = function(obj, propName) {
    return Object.prototype.hasOwnProperty.call(obj, propName) ? obj[propName] : undefined;
};

exports.parseTimeString = function(str) {
    var reg   = /^\s*([1-9]\d*)([dhms])\s*$/;
    var match = str.match(reg);

    if (!match)
        return null;

    var num = parseInt(match[1]);
    switch (match[2]) {
    case 'd': num *= 24;
    case 'h': num *= 60;
    case 'm': num *= 60;
    case 's': num *= 1000;
    }

    assert(num > 0);
    return num;
};

exports.printTimeString = function(ms) {
    var days = Math.ceil(ms / (24*60*60*1000));
    if (days >= 3) return '' + days + 'd';

    var hours = Math.ceil(ms / (60*60*1000));
    if (hours >= 3) return '' + hours + 'h';

    var minutes = Math.ceil(ms / (60*1000));
    if (minutes >= 3) return '' + minutes + 'm';

    var seconds = Math.ceil(ms / 1000);
    return '' + seconds + 's';
};

exports.validateSignature = function(str, sig){
    return exports.sign(str) == sig;
};

exports.removeNullsAndTrim = function(str) {
    if(typeof str === 'string')
        return str.replace(/\0/g, '').trim();
    else
        return str;
};

var letters = "ABCDEFGHIJKLMNOPQRSTUWVXYZ1234567890";

exports.generateSecurityCode = function() {
    var code = "";
    for (var i = 0; i < 5; i++) {
        code += letters.charAt(Math.floor(Math.random() * letters.length));
    }

    return code;
}

exports.generateReferralCode = function(user) {
    var nick = user.nickname.replace(/[^A-Za-z0-9-_]/g, '').substring(0, 3);
    var id = user.steamid.substring(user.steamid.length - 5);

    return nick.toUpperCase() + '-' + id;
}

// must be same on the gameserver
exports.getReferralLevel = function(referred) {
    referred = referred || 0;
    if (referred < 100)
        return 'bronze';
    else if (referred < 200)
        return 'silver';
    else
        return 'gold';
}

// must be same on the gameserver
exports.getReferralProfit = function(referred) {
    referred = referred || 0;
    if (referred < 100)
        return 1 / 300;
    else if (referred < 200)
        return 1 / 200;
    else
        return 1 / 100;
}