var debug    =  require('debug')('app:depositor');
var database = require('./database');
var async    = require('async');
var itemInfo = require('./itemInfo');
var slack    = require('./slack');

function finalizeOffer(offer, status, callback) {
    switch (offer.type) {
        // handle deposits
        case 'deposit':
            if (status === 'completed')
                database.depositCompleted(offer.id, offer.user_id, offer.value, callback);
            else
                database.depositCanceled(offer.id, callback);
            break;

        // handle withdrawals
        case 'withdrawal':
            if (status === 'completed')
                database.withdrawalCompleted(offer.id, offer.user_id, offer.value, callback);
            else
                database.withdrawalCanceled(offer.id, offer.user_id, offer.value, callback);
            break;
    }
}

module.exports.webhook = function (req, res, next) {
    var type = req.query.event;
    // ignore anything that isn't a trade status change
    if (type !== 'trade_status_updated')
        return next();

    var data = req.body;

    function cont (status) {
        // get the offer
        database.getOffer(data.id, function (err, offer) {
            if (err) 
                return res.status(423).end();

            if (offer.status !== 'pending' && offer.status !== 'requested') 
                return res.status(423).end();

            // log into the item watch
            slack.logOffer(offer, status);

            finalizeOffer(offer, status, function (err) {
                if (err && err !== 'ALREADY_PROCESSED')
                    res.status(423);

                next();
            });
        });
    }

    switch (data.status) {
        case 'pending':
            res.status(200).end();
            break;
        // if just made, update the tradeoffer id in the db
        case 'in_progress':
            database.setOfferPending(data.id, data.tradeoffer_url.match(/\d+/)[0], function (err) {
                if (err)
                    res.status(423);

                next();
            });
            break;
        // failed = canceled
        case 'failed': 
        case 'declined':
        case 'canceled':
        case 'timeout':
        case 'denied':
            cont('canceled');
            break;
        // success
        case 'succeeded':
            cont('completed');
            break;
    }
}
