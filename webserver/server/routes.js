var assert = require('better-assert');
var steam = require('steam-login');
var csurf = require('csurf');
var request = require('request');
var passport = require('passport');
var twitchStrategy = require('passport-twitch').Strategy;
var YoutubeV3Strategy = require('passport-youtube-v3').Strategy;
var lib = require('./lib');
var database = require('./database');
var user = require('./user');
var games = require('./games');
var sendEmail = require('./sendEmail');
var stats = require('./stats');
var depositor = require('./depositor');
var config = require('../config/config');

var production = process.env.NODE_ENV === 'production';

function staticPageLogged(page, loggedGoTo) {

    return function(req, res) {
        var user = req.user;
        if (!user){
            return res.render(page);
        }
        if (loggedGoTo) return res.redirect(loggedGoTo);

        res.render(page, {
            user: user
        });
    }
}

function contact(origin) {
    assert(typeof origin == 'string');

    return function(req, res, next) {
        var user = req.user;
        var from = req.body.email;
        var message = req.body.message;

        if (!from ) return res.render(origin, { user: user, warning: 'email required' });

        if (!message) return res.render(origin, { user: user, warning: 'message required' });

        if (user) message = 'user_id: ' + req.user.id + '\n' + message;

        sendEmail.contact(from, message, null, function(err) {
            if (err)
                return next(new Error('Error sending email: \n' + err ));

            return res.render(origin, { user: user, success: 'Thank you for writing, one of my humans will write you back very soon :) ' });
        });
    }
}

function restrict(req, res, next) {
    if (!req.user) {
       res.status(401);
       if (req.header('Accept') === 'text/plain')
          res.send('Not authorized');
       else
          res.render('401');
       return;
    } else
        next();
}

function restrictRedirectToHome(req, res, next) {
    if(!req.user) {
        res.redirect('/');
        return;
    }
    next();
}

function adminRestrict(req, res, next) {

    if (!req.user || !req.user.admin) {
        res.status(401);
        if (req.header('Accept') === 'text/plain')
            res.send('Not authorized');
        else
            res.render('401'); //Not authorized page.
        return;
    }
    next();
}

function index() {
    return function(req, res) {
        var user = req.user;
        database.getServers(function (err, servers) {
            if (err) return res.status(500);

            if (!user) {
                return res.render('index', {
                    servers: servers
                });
            }

            res.render('index', {
                user: user,
                servers: servers
            });
        });
    }
}

function tableNew() {
    return function(req, res) {
        var id = parseInt(req.params.server) || 1;
        console.log("ID: " + id);
        database.getServer(id, function (err, server) {
            if (err) return res.status(500);
            if (!server || !server.active) return res.status(404).render(404);
			console.log(server.host);
			console.log(config.BUILD);
            res.render('table_new', {
                user: req.user,
                host: server.host,
                buildConfig: config.BUILD,
                table: true
            });
        });
    }
}

function tableDev() {
    return function(req, res) {
        if(config.PRODUCTION)
            return res.status(401);
        requestDevOtt(req.params.id, function(devOtt) {
            res.render('table_new', {
                user: req.user,
                devOtt: devOtt,
                table: true
            });
        });
    }
}
function requestDevOtt(id, callback) {
    var curl = require('curlrequest');
    var options = {
        url: config.SITE_URL + '/ott',
        include: true ,
        method: 'POST',
        'cookie': 'id='+id
    };

    var ott=null;
    curl.request(options, function (err, parts) {
        parts = parts.split('\r\n');
        var data = parts.pop()
            , head = parts.pop();
        ott = data.trim();
        console.log('DEV OTT: ', ott);
        callback(ott);
    });
}

var csrfProtect = csurf({cookie: true});

module.exports = function(app) {
    app.get('/', index());
    app.get('/faq', staticPageLogged('faq'));
    app.get('/contact', staticPageLogged('contact'));
    app.get('/request', restrict, user.request);
    app.get('/deposit', restrict, csrfProtect, user.deposit);
    app.get('/withdraw', restrict, csrfProtect, user.withdraw);
    app.get('/transfer', restrict, user.transfer);
    app.get('/transfer.json', restrict, user.transferJson);
    app.get('/transfer/request', restrict, csrfProtect, user.transferRequest);
    app.get('/support', restrict, csrfProtect, user.contact);
    app.get('/account', restrict, csrfProtect, user.account);
    app.get('/referral', restrict, csrfProtect, user.referrals);
    app.get('/connections', restrict, csrfProtect, user.connections);
    app.get('/calculator', staticPageLogged('calculator'));
    app.get('/guide', staticPageLogged('guide'));

    app.get('/play/:server?', tableNew());

    app.get('/leaderboard', games.getLeaderBoard);
    app.get('/game/:server/:id.json', games.getGameInfoJson);
    app.get('/game/:server/:id', games.show);
    app.get('/user/:steamid', user.profile);
    app.get('/error', function(req, res, next) { // Sometimes we redirect people to /error
      return res.render('error');
    });

    app.post('/transfer-request', restrict, csrfProtect, user.handleTransferRequest);
    // app.post('/deposit-request', restrict, csrfProtect, user.handleDepositRequest);
    // app.post('/withdraw-request', restrict, csrfProtect, user.handleWithdrawRequest);
    app.post('/account/tradelink', restrict, csrfProtect, user.setTradeLink);
    app.post('/support', restrict, csrfProtect, contact('support'));
    app.post('/contact', contact('contact'));
    app.post('/logout', restrictRedirectToHome, user.logout);

    app.post('/webhook/' + config.STEAMTRADES_WEBHOOK_SECRET, depositor.webhook, user.webhook);

    app.get('/withdraw/items.json', restrict, csrfProtect, user.itemsJson);
    app.get('/deposit/items.json', restrict, csrfProtect, user.inventoryJson);

    // app.get('/deposit/items/status.json', restrict, user.inventoryStatusJson);

    // app.get('/withdraw/status.json', restrict, user.tradeStatusJson);
    // app.get('/deposit/status.json', restrict, user.tradeStatusJson);

    app.post('/deposit.json', restrict, csrfProtect, user.depositJson);
    app.post('/withdraw.json', restrict, csrfProtect, user.withdrawJson);
    
    app.post('/referral/use', restrict, csrfProtect, user.referralsUseCode);
    app.post('/referral/generate', restrict, csrfProtect, user.referralsGenerateCode);
console.log(config.LOGIN_URL);
console.log(config.LOGIN_URL);
console.log(config.STEAM_API_KEY);

    app.use(steam.middleware({
        realm: config.LOGIN_URL + '/',
        verify: config.LOGIN_URL + '/login/verify',
        apiKey: config.STEAM_API_KEY,

        useSession: false // we're using our own session system
    }));

    app.use(passport.initialize({
        userProperty: 'connectionProfile'
    }));
    passport.use(new twitchStrategy({
        clientID: '4a1knfgbmy9f76udggcwd87bqrdl8lb',
        clientSecret: 'rkir549bdkqds8lzvzg3xvtw6nbqffb',
        scope: 'user_read',
        callbackURL: config.SITE_URL + '/connection/twitch/verify'
    }, function(accessToken, refreshToken, profile, done) {
        request.get('https://api.twitch.tv/kraken/channels/' + profile.username, {json: true}, function(err, response, body) {
            if (err) return done(err);
            if (response.statusCode !== 200) return done(new Error(response.statusCode));

            done(null, {
                name: profile.username,
                followers: body.followers,
                views: body.views,
                profile: profile._json
            });
        });
    }));

    app.get('/connection/twitch/auth', passport.authenticate('twitch'));
    app.get('/connection/twitch/verify', passport.authenticate('twitch', {session: false}), user.twitchConnection);

    passport.use(new YoutubeV3Strategy({
        clientID: '120212943522-emegkseffrulah7je4n4mcgddollqb19.apps.googleusercontent.com',
        clientSecret: 'VgFPzP3eQprCU0fALSkvq9yX',
        scope: ['https://www.googleapis.com/auth/youtube.readonly'],
        callbackURL: config.SITE_URL + '/connection/youtube/verify'
    }, function(accessToken, refreshToken, profile, done) {
        console.log(profile);
        this._oauth2.getProtectedResource('https://www.googleapis.com/youtube/v3/channels?part=statistics&mine=true', accessToken, function (err, body, res) {
            if (err) return done(new Error('Failed to fetch channel statistics:\n' + err));

            try {
                var json = JSON.parse(body);

                done(null, {
                    name: profile.displayName,
                    followers: json.items[0].statistics.subscriberCount || 0,
                    views: json.items[0].statistics.viewCount || 0,
                    profile: profile
                });
            } catch(e) {
                done(e);
            }
        })
    }));

    app.get('/connection/youtube/auth', passport.authenticate('youtube'));
    app.get('/connection/youtube/verify', passport.authenticate('youtube', {session: false}), user.youtubeConnection);

    app.get('/login', steam.authenticate(), function(req, res) {
		console.log(req, res);
        res.redirect('/');
    });
    app.get('/login/verify', steam.verify(), user.steamLogin);
    app.get('/login/redirect', user.loginRedirect);

    app.post('/ott', restrict, function(req, res, next) {
        var user = req.user;
        var ipAddress = req.ip;
        var userAgent = req.get('user-agent');
        assert(user);
        
        database.createOneTimeToken(user.id, ipAddress, userAgent, function(err, token) {
            if (err) {
                console.error('[INTERNAL_ERROR] unable to get OTT got ' + err);
                res.status(500);
                return res.send('Server internal error');
            }
            res.send(token);
        });
    });

    app.get('/stats', stats.index);

    app.get('*', function(req, res) {
        res.status(404);
        res.render('404');
    });
};
