var Slack    = require('node-slack');
var async    = require('async');
var database = require('./database');
var config   = require('../config/config');
var itemInfo = require('./itemInfo');

var client = new Slack(config.SLACK_WEBHOOK_URL);

module.exports.logOffer = function (offer, status) {
    if (status !== 'completed') return;

    async.parallel({
        user: function (callback) {
            // get the user
            database.getUserByID(offer.user_id, callback);
        },
        items: function (callback) {
            // get offer id
            database.getItemsByOffer(offer.id, callback);
        }
    }, function (err, results) {
        if (err) {
            console.error('Cannot log offer id ' + offer.id + ' into slack! Database error:', err);
            return;
        }

        var attachments = results.items.map(function (item) {
            var price = itemInfo.getPrice(item.name) / 100 / 1000;
            return {
                fallback: item.name,
                color: price > 80 ? 'danger' : 'good',
                text: '*$' + price.toFixed(2) + '* - ' + item.name + ' (id ' + item.id + ')'
            };
        });

        client.send({
            channel: '#itemwatch',
            username: results.user.nickname + ' (' + results.user.steamid + ')',
            icon_url: results.user.avatar,
            text: ':heavy_' + (offer.type == 'deposit' ? 'plus' : 'minus') + '_sign: made a ' + offer.type + ' valued at *$' + (offer.value / 100 / 1000).toFixed(2) + '*',
            attachments: attachments
        });
    });
}

module.exports.logOfferError = function (offer_id, info, err) {
    var error = err.message ? err.message : err;
    client.send({
        channel: '#errorwatch',
        username: 'Error Watch',
        icon_emoji: 'mag',
        text: '*Offer ' + offer_id + '*: ' + info,
        attachments: [
            {
                fallback: error,
                text: error,
                color: 'danger'
            }
        ]
    });
}
