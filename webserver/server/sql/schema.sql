DROP SCHEMA public CASCADE;
CREATE SCHEMA public;

CREATE EXTENSION IF NOT EXISTS plv8;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";


-- Users

CREATE TYPE UserClassEnum AS ENUM ('user', 'twitch', 'youtube', 'moderator', 'admin');


CREATE TABLE users (
    id bigint NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    steamid text NOT NULL,
    nickname text,
    avatar text,
    balance bigint DEFAULT 0 NOT NULL,
    gross_profit bigint DEFAULT 0 NOT NULL,
    net_profit bigint DEFAULT 0 NOT NULL,
    games_played bigint DEFAULT 0 NOT NULL,
    trade_link text,
    referrer_id bigint,-- REFERENCES users(id),
    referred bigint DEFAULT 0 NOT NULL,
    referral_code text,
    referred_time timestamp with time zone DEFAULT now() NOT NULL,
    referral_profit bigint DEFAULT 0 NOT NULL,
    verified boolean DEFAULT false, -- user is verified once he makes his first deposit
    userclass UserClassEnum DEFAULT 'user' NOT NULL,
    CONSTRAINT users_balance_check CHECK ((balance >= 0))
);

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);

CREATE UNIQUE INDEX unique_steamid ON users USING btree (steamid);
CREATE UNIQUE INDEX unique_referral_code ON users USING btree (referral_code);
CREATE INDEX users_referrer_id_idx ON users USING btree (referrer_id, referral_profit DESC);
CREATE INDEX user_id_idx ON users USING btree (id);

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE users_id_seq OWNED BY users.id;

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


-- Transfers (Tips)
CREATE TABLE transfers (
  id uuid NOT NULL PRIMARY KEY,
  from_user_id bigint NOT NULL REFERENCES users(id),
  to_user_id bigint NOT NULL  REFERENCES users(id),
  amount bigint NOT NULL,
  created timestamp with time zone DEFAULT now() NOT NULL,
  CONSTRAINT user_transfer_valid_amount CHECK(amount>0)
);

CREATE INDEX transfer_from_user_id_idx ON transfers USING btree (from_user_id, created);
CREATE INDEX transfer_to_user_id_idx ON transfers USING btree (to_user_id, created);


CREATE TYPE OfferTypeEnum AS ENUM ('deposit', 'withdrawal');
CREATE TYPE OfferStatusEnum AS ENUM ('requested', 'pending', 'canceled', 'completed');

CREATE TABLE offers (
    id bigint NOT NULL PRIMARY KEY,
    tradeoffer_id bigint,
    security_code text NOT NULL,
    user_id bigint NOT NULL references users(id),
    value bigint,
    type OfferTypeEnum NOT NULL,
    status OfferStatusEnum DEFAULT 'requested' NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT offers_tradeoffer_id_key UNIQUE (tradeoffer_id)
);

-- Items
CREATE TABLE items (
    id bigint NOT NULL PRIMARY KEY, -- id
    name text NOT NULL,
    deposit_id bigint REFERENCES offers(id),
    withdrawal_id bigint REFERENCES offers(id),
    received boolean DEFAULT false
);

-- Fundings
CREATE TABLE fundings (
    id bigserial NOT NULL PRIMARY KEY,
    user_id bigint NOT NULL REFERENCES users(id),
    amount bigint NOT NULL,
    offer_id bigint REFERENCES offers(id),
    created timestamp with time zone DEFAULT now() NOT NULL,
    description text
);

ALTER TABLE ONLY fundings
    ADD CONSTRAINT fundings_user_id_tradeoffer_id_key UNIQUE (user_id, offer_id);

CREATE INDEX fundings_user_id_idx ON fundings USING btree (user_id);

-- Games

CREATE TABLE games (
    id bigint NOT NULL,
    game_crash bigint NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    ended boolean DEFAULT false NOT NULL,
    server_id int NOT NULL DEFAULT 1
);

ALTER TABLE ONLY games ADD CONSTRAINT games_pkey PRIMARY KEY (id, server_id);

-- CREATE SEQUENCE games_id_seq
--     START WITH 1
--     INCREMENT BY 1
--     NO MINVALUE
--     NO MAXVALUE
--     CACHE 1;

-- ALTER SEQUENCE games_id_seq OWNED BY games.id;

-- ALTER TABLE ONLY games ALTER COLUMN id SET DEFAULT nextval('games_id_seq'::regclass);


-- Plays

CREATE TABLE plays (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    cash_out bigint,
    auto_cash_out bigint NOT NULL,
    game_id bigint NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    bet bigint NOT NULL,
    bonus bigint,
    server_id int NOT NULL DEFAULT 1
);

ALTER TABLE ONLY plays ADD CONSTRAINT plays_pkey PRIMARY KEY (id);

CREATE INDEX plays_game_id_idx ON plays USING btree (game_id, server_id);

CREATE INDEX plays_user_id_idx ON plays USING btree (server_id, user_id, id DESC);

ALTER TABLE ONLY plays ADD CONSTRAINT plays_game_id_fkey FOREIGN KEY (game_id, server_id) REFERENCES games(id, server_id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE ONLY plays ADD CONSTRAINT plays_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;

CREATE SEQUENCE plays_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE plays_id_seq OWNED BY plays.id;

ALTER TABLE ONLY plays ALTER COLUMN id SET DEFAULT nextval('plays_id_seq'::regclass);




-- Sessions:
    -- Regular sessions for users and one time tokens for the cross origin connection to the game server
    -- Ott allows to let the session is http only

CREATE TABLE sessions (
    id uuid NOT NULL,
    user_id bigint NOT NULL,
    ip_address inet,-- NOT NULL,
    user_agent text,
    fingerprint text, -- The fingerprint of the browser that created this session
    ott boolean DEFAULT false,
    created timestamp with time zone NOT NULL DEFAULT now(),
    expired timestamp with time zone NOT NULL DEFAULT now() + interval '21 days'
);

ALTER TABLE ONLY sessions
    ADD CONSTRAINT unique_id PRIMARY KEY (id);

CREATE INDEX sessions_user_id_idx ON sessions USING btree (user_id, expired);


CREATE TABLE connections (
    id bigserial NOT NULL PRIMARY KEY,
    user_id bigint NOT NULL REFERENCES users(id),
    service text NOT NULL,
    identifier text NOT NULL,
    token text,
    profile json
);

ALTER TABLE ONLY connections
    ADD CONSTRAINT connections_user_id_service_key UNIQUE (user_id, service);

CREATE INDEX connections_user_id_idx ON connections USING btree (user_id);

-- Users View

CREATE VIEW users_view AS
 SELECT u.id,
    u.created,
    u.steamid,
    u.nickname,
    u.avatar,
    u.balance,
    u.games_played,
    u.trade_link,
    u.userclass,
    u.verified,
    u.referred,
    u.referrer_id,
    u.referral_code
   FROM users u;



CREATE TABLE game_hashes
(
 game_id bigint NOT NULL,
 hash text NOT NULL,
 server_id int NOT NULL,
 CONSTRAINT game_hashes_pkey PRIMARY KEY (game_id, server_id)
);



-- Leaderboard View

CREATE MATERIALIZED VIEW leaderboard AS
 SELECT id as user_id,
        nickname,
        steamid,
        gross_profit,
        net_profit,
        games_played,
        rank() OVER (ORDER BY gross_profit DESC) AS rank
   FROM users;

CREATE UNIQUE INDEX leaderboard_user_id_idx
  ON leaderboard
  USING btree
  (user_id);

CREATE INDEX leaderboard_steamid_idx ON leaderboard USING btree (steamid);

CREATE INDEX leaderboard_gross_profit_idx ON leaderboard USING btree (gross_profit);

CREATE INDEX leaderboard_net_profit_idx ON leaderboard USING btree (net_profit);



-- Chat messages

CREATE TABLE chat_messages
(
  id bigserial NOT NULL PRIMARY KEY,
  user_id bigint NOT NULL REFERENCES users(id),
  message text NOT NULL,
  created timestamp with time zone DEFAULT now() NOT NULL,
  is_bot boolean NOT NULL,
  channel text NOT NULL
);

CREATE INDEX chat_messages_user_id_idx ON chat_messages USING btree(user_id);
CREATE INDEX chat_messages_channel_id_idx ON chat_messages USING btree(channel, id);


-- Player inventories
CREATE TABLE inventories
(
  id bigserial NOT NULL PRIMARY KEY,
  user_id bigint NOT NULL REFERENCES users(id),
  retrieved timestamp with time zone DEFAULT now() NOT NULL,
  items json,
  error text,
  requests integer NOT NULL DEFAULT 1
);

CREATE INDEX inventories_user_id_idx ON inventories USING btree(user_id);

-- Servers

CREATE TABLE servers
(
  id int NOT NULL PRIMARY KEY,
  host text NOT NULL,
  players int NOT NULL,
  active boolean DEFAULT true NOT NULL
);

-- User stats

CREATE OR REPLACE FUNCTION plays_users_stats_trigger()
  RETURNS trigger AS $$

    if (TG_OP === 'UPDATE' && OLD.user_id !== NEW.user_id)
      throw new Error('Update of user_id not allowed');

    var userId, gross = 0, net = 0, num = 0;
    var bet, cashOut, bonus;

    // Add new values.
    if (NEW) {
      userId  = NEW.user_id;
      bet     = NEW.bet;
      bonus   = NEW.bonus || 0;
      cashOut = NEW.cash_out || 0;

      gross  += Math.max(cashOut - bet, 0) + bonus;
      net    += (cashOut - bet) + bonus;
      num    += 1;
    }

    // Subtract old values
    if (OLD) {
      userId  = OLD.user_id;
      bet     = OLD.bet;
      bonus   = OLD.bonus || 0;
      cashOut = OLD.cash_out || 0;

      gross  -= Math.max(cashOut - bet, 0) + bonus;
      net    -= (cashOut - bet) + bonus;
      num    -= 1;
    }

    var sql =
      'UPDATE users ' +
      '  SET gross_profit = gross_profit + $1, ' +
      '      net_profit   = net_profit   + $2, ' +
      '      games_played = games_played + $3 ' +
      '  WHERE id = $4';
    var par = [gross,net,num,userId];
    plv8.execute(sql,par);
$$ LANGUAGE plv8;

CREATE TRIGGER plays_users_stats_trigger
AFTER INSERT OR UPDATE OR DELETE ON plays
    FOR EACH ROW EXECUTE PROCEDURE plays_users_stats_trigger();

CREATE FUNCTION ip_root(ip_address inet) RETURNS inet AS $$
  BEGIN
    RETURN host(network(set_masklen(ip_address, (CASE family(ip_address) WHEN 4 THEN 24 ELSE 48 END))));
  END;
$$ LANGUAGE plpgsql IMMUTABLE;
