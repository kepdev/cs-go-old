var assert = require('assert');
var uuid = require('uuid');
var config = require('../config/config');

var async = require('async');
var lib = require('./lib');
var pg = require('pg');
var m = require('multiline');

var databaseUrl = config.DATABASE_URL;

if (!databaseUrl)
    throw new Error('must set DATABASE_URL environment var');

console.log('DATABASE_URL: ', databaseUrl);

// sure why not
pg.defaults.poolSize = 40;

pg.types.setTypeParser(20, function(val) { // parse int8 as an integer
    return val === null ? null : parseInt(val);
});

// callback is called with (err, client, done)
function connect(callback) {
    return pg.connect(databaseUrl, callback);
}

function query(query, params, callback) {
    //third parameter is optional
    if (typeof params == 'function') {
        callback = params;
        params = [];
    }

    doIt();
    function doIt() {
        connect(function(err, client, done) {
            if (err) return callback(err);
            client.query(query, params, function(err, result) {
                done();
                if (err) {
                    if (err.code === '40P01') {
                        console.error('[INTERNAL] Warning: Retrying deadlocked transaction: ', query, params);
                        return doIt();
                    }
                    return callback(err);
                }

                callback(null, result);
            });
        });
    }
}

exports.query = query;

pg.on('error', function(err) {
    console.error('POSTGRES EMITTED AN ERROR', err);
});


// runner takes (client, callback)

// callback should be called with (err, data)
// client should not be used to commit, rollback or start a new transaction

// callback takes (err, data)

function getClient(runner, callback) {
    doIt();

    function doIt() {
        connect(function (err, client, done) {
            if (err) return callback(err);

            function rollback(err) {
                client.query('ROLLBACK', done);

                if (err.code === '40P01') {
                    console.error('[INTERNAL_ERROR] Warning: Retrying deadlocked transaction..');
                    return doIt();
                }

                callback(err);
            }

            client.query('BEGIN', function (err) {
                if (err)
                    return rollback(err);

                runner(client, function (err, data) {
                    if (err)
                        return rollback(err);

                    client.query('COMMIT', function (err) {
                        if (err)
                            return rollback(err);

                        done();
                        callback(null, data);
                    });
                });
            });
        });
    }
}

// refresh leaderboard
function refreshView() {

    query('REFRESH MATERIALIZED VIEW CONCURRENTLY leaderboard;', function(err) {
        if (err) {
            console.error('[INTERNAL_ERROR] unable to refresh leaderboard got: ', err);
        } else {
            console.log('leaderboard refreshed');
        }

        setTimeout(refreshView, 10 * 60 * 1000);
    });

}
setTimeout(refreshView, 10000); // schedule it so it comes after all the addresses are generated

//Returns a sessionId
exports.registerOrLogin = function(steamid, nickname, avatar, callback) {
    assert(steamid && nickname && avatar);

    getClient(
        function(client, callback) {
            client.query('SELECT COUNT(*) count FROM users WHERE steamid = $1', [steamid],
                function(err, data) {
                    if (err) return callback(err);
                    assert(data.rows.length === 1);
                    // updates and logs-in already registered user
                    function updateUser(nickname, avatar, steamid, callback) {
                        client.query('UPDATE users SET nickname = $1, avatar = $2 WHERE steamid = $3 RETURNING id', [nickname, avatar, steamid],
                            function(err, data) {
                                if (err) return callback(err);

                                assert(data.rows.length === 1);
                                var user = data.rows[0];

                                createSession(client, user.id, callback);
                            });
                    }
                    // already registered, update nickname and avatar
                    if (data.rows[0].count > 0) {
                        return updateUser(nickname, avatar, steamid, callback);
                    }

                    client.query('INSERT INTO users(steamid, nickname, avatar) VALUES($1, $2, $3) RETURNING id',
                            [steamid, nickname, avatar],
                            function(err, data) {
                                if (err)  {
                                    if (err.code === '23505') {
                                        // already registered (again???), update nickname and avatar
                                        return updateUser(nickname, avatar, steamid, callback);
                                    } else {
                                        return callback(err);
                                    }
                                }

                                assert(data.rows.length === 1);
                                var user = data.rows[0];

                                createSession(client, user.id, callback);
                            }
                        );

                    });
        }
    , callback);
};

exports.setTradeLink = function(userId, link, callback) {
    assert(userId);
    assert(link);
    query('UPDATE users SET trade_link = $1 WHERE id = $2', [link, userId], function(err) {
        if (err) return callback(err);

        callback(null);
    });
}

/** Expire all the not expired sessions of an user by id **/
exports.expireSessionsByUserId = function(userId, callback) {
    assert(userId);

    query('UPDATE sessions SET expired = now() WHERE user_id = $1 AND expired > now()', [userId], callback);
};


function createSession(client, userId, callback) {
    var sessionId = uuid.v4();

    var expired = new Date();
    expired.setDate(expired.getDate() + 21);

    client.query('INSERT INTO sessions(id, user_id, expired) VALUES($1, $2, $3) RETURNING id',
        [sessionId, userId, expired], function(err, res) {
        if (err) return callback(err);
        assert(res.rows.length === 1);

        var session = res.rows[0];
        assert(session.id);

        callback(null, session.id, expired);
    });
}

exports.createOneTimeToken = function(userId, ipAddress, userAgent, callback) {
    assert(userId);
    var id = uuid.v4();

    query('INSERT INTO sessions(id, user_id, ip_address, user_agent, ott) VALUES($1, $2, $3, $4, true) RETURNING id', [id, userId, ipAddress, userAgent], function(err, result) {
        if (err) return callback(err);
        assert(result.rows.length === 1);

        var ott = result.rows[0];

        callback(null, ott.id);
    });
};

exports.getUserBySessionId = function(sessionId, callback) {
    assert(sessionId && callback);
    query('SELECT * FROM users_view WHERE id = (SELECT user_id FROM sessions WHERE id = $1 AND ott = false AND expired > now())', [sessionId], function(err, response) {
        if (err) return callback(err);

        var data = response.rows;
        if (data.length === 0)
            return callback('NOT_VALID_SESSION');

        assert(data.length === 1);

        var user = data[0];
        assert(typeof user.balance === 'number');

        callback(null, user);
    });
};

exports.getUserBySteamID = function(steamid, callback) {
    assert(steamid);
    query('SELECT * FROM users WHERE steamid = $1', [steamid], function(err, result) {
        if (err) return callback(err);
        if (result.rows.length === 0)
            return callback('USER_DOES_NOT_EXIST');

        assert(result.rows.length === 1);
        callback(null, result.rows[0]);
    });
};

exports.getUserByID = function(id, callback) {
    assert(id);
    query('SELECT * FROM users WHERE id = $1', [id], function(err, result) {
        if (err) return callback(err);
        if (result.rows.length === 0)
            return callback('USER_DOES_NOT_EXIST');

        assert(result.rows.length === 1);
        callback(null, result.rows[0]);
    });
};

exports.getGame = function(gameId, serverId, callback) {
    assert(gameId && callback);

    query('SELECT * FROM games ' +
    'LEFT JOIN game_hashes ON games.id = game_hashes.game_id AND games.server_id = game_hashes.server_id ' +
    'WHERE games.id = $1 AND games.server_id = $2 AND games.ended = TRUE', [gameId, serverId], function(err, result) {
        if (err) return callback(err);
        if (result.rows.length == 0) return callback('GAME_DOES_NOT_EXISTS');
        assert(result.rows.length == 1);
        callback(null, result.rows[0]);
    });
};

exports.getGamesPlays = function(gameId, serverId, callback) {
    query('SELECT u.steamid, u.nickname, p.bet, p.cash_out, p.bonus FROM plays p, users u ' +
        ' WHERE game_id = $1 AND server_id = $2 AND p.user_id = u.id ORDER by p.cash_out/p.bet::float DESC NULLS LAST, p.bet DESC', [gameId, serverId],
        function(err, result) {
            if (err) return callback(err);
            return callback(null, result.rows);
        }
    );
};

exports.getGameInfo = function(gameId, serverId, callback) {
    assert(gameId && callback);

    var gameInfo = { game_id: gameId, server_id: serverId };

    function getSqlGame(callback) {

        var sqlGame = m(function() {/*
         SELECT game_crash, created, hash
         FROM games LEFT JOIN game_hashes ON games.id = game_id
         WHERE games.ended = true AND games.id = $1 AND games.server_id = $2
         */});

        query(sqlGame, [gameId, serverId], function(err, result) {
            if(err)
                return callback(err);

            if (result.rows.length === 0)
                return callback('GAME_DOES_NOT_EXISTS');

            console.assert(result.rows.length === 1);

            var game = result.rows[0];

            gameInfo.game_crash = game.game_crash;
            gameInfo.hash = game.hash;
            gameInfo.created = game.created;

            callback(null);
        });
    }

    function getSqlPlays(callback) {
        var sqlPlays = m(function() {/*
         SELECT steamid, nickname, bet, (100 * cash_out / bet)::bigint AS stopped_at, bonus
         FROM plays JOIN users ON user_id = users.id WHERE game_id = $1 AND server_id = $2
         */});

        query(sqlPlays, [gameId, serverId], function(err, result) {
            if(err)
                return callback(err);

            var playsArr = result.rows;

            var player_info = {};
            playsArr.forEach(function(play) {
                player_info[play.steamid] = {
                    nickname: play.nickname,
                    bet: play.bet,
                    stopped_at: play.stopped_at,
                    bonus: play.bonus
                };
            });

            gameInfo.player_info = player_info;

            callback(null);
        });
    }


    async.parallel([getSqlGame, getSqlPlays],
    function(err, results) {
        if(err)
            return callback(err);

        callback(null, gameInfo);
    });
};

exports.addBalance = function(userId, amount, callback) {
    assert(userId);
    assert(amount);
    
    query('UPDATE users SET balance = balance + $1 WHERE id = $2', [amount, userId], function(err, res) {
        if (err) return callback(err);
        assert(res.rowCount === 1);
        callback(null);
    });
}

exports.getUserPlays = function(userId, limit, offset, callback) {
    assert(userId);

    query('SELECT p.bet, p.bonus, p.cash_out, p.created, p.game_id, g.game_crash, p.server_id FROM plays p ' +
        'LEFT JOIN (SELECT * FROM games) g ON g.id = p.game_id AND g.server_id = p.server_id ' +
        'WHERE p.user_id = $1 AND g.ended = true ORDER BY p.created DESC LIMIT $2 OFFSET $3',
        [userId, limit, offset], function(err, result) {
            if (err) return callback(err);
            callback(null, result.rows);
        }
    );
};

exports.getUserNetProfit = function(userId, callback) {
    assert(userId);
    query('SELECT net_profit AS profit FROM users WHERE id = $1', [userId], function(err, result) {
            if (err) return callback(err);
            assert(result.rows.length == 1);
            return callback(null, result.rows[0]);
        }
    );
};

exports.getUserNetProfitLast = function(userId, last, callback) {
    assert(userId);
    query('SELECT (' +
            'COALESCE(SUM(cash_out), 0) + ' +
            'COALESCE(SUM(bonus), 0) - ' +
            'COALESCE(SUM(bet), 0))::bigint profit ' +
            'FROM ( ' +
                'SELECT * FROM plays ' +
                'WHERE user_id = $1 ' +
                'ORDER BY id DESC ' +
                'LIMIT $2 ' +
            ') restricted ', [userId, last], function(err, result) {
            if (err) return callback(err);
            assert(result.rows.length == 1);
            return callback(null, result.rows[0].profit);
        }
    );
};

exports.getPublicStats = function(steamid, callback) {

  var sql = 'SELECT id AS user_id, steamid, nickname, avatar, gross_profit, net_profit, games_played, ' +
            'COALESCE((SELECT rank FROM leaderboard WHERE user_id = id), -1) rank ' +
            'FROM users WHERE steamid = $1';

    query(sql,
        [steamid], function(err, result) {
            if (err) return callback(err);

            if (result.rows.length !== 1)
                return callback('USER_DOES_NOT_EXIST');

            return callback(null, result.rows[0]);
        }
    );
};

exports.makeTransfer = function(uid, fromUserId, toSteamId, amount, callback){
    assert(typeof fromUserId === 'number');
    assert(typeof toSteamId === 'string');
    assert(typeof amount === 'number');

    // Update balances
    getClient(function(client, callback) {
        async.waterfall([
            function(callback) {
                client.query("UPDATE users SET balance = balance - $1 WHERE id = $2",
                  [amount, fromUserId], callback)
            },
            function(prevData, callback) {
                client.query(
                    "UPDATE users SET balance = balance + $1 WHERE steamid = $2 RETURNING id",
                    [amount, toSteamId], function(err, data) {
                        if (err)
                            return callback(err);
                        if (data.rowCount === 0)
                          return callback('USER_NOT_EXIST');
                        var toUserId = data.rows[0].id;
                        assert(Number.isInteger(toUserId));
                        callback(null, toUserId);
                  });
            },
            function (toUserId, callback) {
                client.query(
                  "INSERT INTO transfers (id, from_user_id, to_user_id, amount) values($1,$2,$3,$4) ",
                  [uid, fromUserId, toUserId, amount], callback);
            }
        ], function(err) {
            if (err) {
                if (err.code === '23514') {// constraint violation
                    return callback('NOT_ENOUGH_BALANCE');
                }
                if (err.code === '23505') { // dupe key
                    return callback('TRANSFER_ALREADY_MADE');
                }

                return callback(err);
            }
            callback();
        });
    }, callback);

};

exports.getWithdrawals = function(userId, callback) {
    assert(userId && callback);

    query("SELECT * FROM offers WHERE user_id = $1 AND type = $2 ORDER BY created DESC", [userId, 'withdrawal'], function(err, result) {
        if (err) return callback(err);

        var data = result.rows.map(function(row) {
            return {
                value: row.value,
                tradeoffer_id: row.tradeoffer_id,
                security_code: row.security_code,
                status: row.status,
                created: row.created
            };
        });
        callback(null, data);
    });
};

exports.getTransfers = function (userId, callback){
    assert(userId);
    assert(callback);

    var sql = m(function() {/*
        SELECT
           transfers.id,
           transfers.amount,
           transfers.created,
           transfers.to_user_id,
           transfers.from_user_id,
           users.steamid as steamid,
           users.nickname as nickname,
           ($1 = transfers.from_user_id) as is_from_me
        FROM transfers
        JOIN users ON ((transfers.from_user_id = $1 AND transfers.to_user_id = users.id) OR (transfers.to_user_id = $1 AND transfers.from_user_id = users.id))
        WHERE from_user_id = $1
           OR   to_user_id = $1
        ORDER by transfers.created DESC
        LIMIT 25
    */});

    query(sql, [userId], function(err, data) {
        if (err)
            return callback(err);

        callback(null, data.rows);
    });
};

exports.getDeposits = function(userId, callback) {
    assert(userId && callback);

    query("SELECT * FROM offers WHERE user_id = $1 AND type = $2 ORDER BY created DESC", [userId, 'deposit'], function(err, result) {
        if (err) return callback(err);

        var data = result.rows.map(function(row) {
            return {
                value: row.value,
                tradeoffer_id: row.tradeoffer_id,
                security_code: row.security_code,
                status: row.status,
                created: row.created
            };
        });
        callback(null, data);
    });
};

exports.getOffer = function(offerId, callback) {
    assert(offerId);

    query('SELECT * FROM offers WHERE id = $1', [offerId], function (err, data) {
        if (err)
            return callback(err);
        if (data.rows.length !== 1)
            return callback('NOT_FOUND');

        callback(null, data.rows[0]);
    });
}

exports.depositCanceled = function(offerId, callback) {
    getClient(function(client, callback) {
        async.parallel([
            function(callback) {
                // mark as canceled
                client.query("UPDATE offers SET status = 'canceled' WHERE id = $1 AND (status = 'pending' OR status = 'requested')", [offerId], function (err, res) {
                    if (err) return callback(err);

                    if (res.rowCount !== 1) return callback('ALREADY_PROCESSED');

                    callback();
                });
            },
            function(callback) {
                // remove items
                client.query('DELETE FROM items WHERE deposit_id = $1', [offerId], callback);
            }
        ], callback);
    }, function(err) {
        if (err) {
            console.log('[INTERNAL_ERROR] could not cancel deposit: (', offerId, ') got err: ', err);
            return callback(err);
        }

        callback(null);
    });
}

exports.withdrawalCanceled = function(offerId, userId, value, callback) {
    getClient(function(client, callback) {
        async.parallel([
            function(callback) {
                // mark as canceled
                client.query("UPDATE offers SET status = 'canceled' WHERE id = $1 AND (status = 'pending' OR status = 'requested')", [offerId], function (err, res) {
                    if (err) return callback(err);

                    if (res.rowCount !== 1) return callback('ALREADY_PROCESSED');

                    callback();
                });
            },
            function(callback) {
                // add balance back
                client.query("UPDATE users SET balance = balance + $1 WHERE id = $2", [value, userId], callback);
            },
            function(callback) {
                // free the items
                client.query('UPDATE items SET withdrawal_id = NULL WHERE withdrawal_id = $1', [offerId], callback);
            }
        ], callback);
    }, function(err) {
        if (err) {
            console.log('[INTERNAL_ERROR] could not cancel withdraw: (', offerId, ') got err: ', err);
            return callback(err);
        }

        callback();
    });
}

exports.depositCompleted = function(offerId, userId, value, callback) {
    getClient(function(client, callback) {
        async.parallel([
            function(callback) {
                // mark as done
                client.query("UPDATE offers SET status = 'completed' WHERE id = $1 AND (status = 'pending' OR status = 'requested')", [offerId], function (err, res) {
                    if (err) return callback(err);

                    if (res.rowCount !== 1) return callback('ALREADY_PROCESSED');

                    callback();
                });
            },
            function(callback) {
                // add the funding
                client.query("INSERT INTO fundings(offer_id, user_id, amount, description) VALUES($1, $2, $3, 'Skin deposit')", [offerId, userId, value], callback)
            },
            function(callback) {
                // mark items as received
                client.query('UPDATE items SET received = true WHERE deposit_id = $1', [offerId], callback);
            },
            function(callback) {
                // add balance
                client.query("UPDATE users SET balance = balance + $1 WHERE id = $2", [value, userId], callback);
            },
            function(callback) {
                // set user to verified
                client.query("UPDATE users SET verified = true WHERE verified = false AND id = $1 RETURNING referrer_id", [userId], function (err, res) {
                    if (err) return callback(err);

                    if (res.rowCount === 1) {
                        client.query('UPDATE users SET referred = referred + 1 WHERE id = $1', [res.rows[0].referrer_id], callback);
                    } else {
                        callback();
                    }
                });
            }
        ], callback);
    }, function(err) {
        if (err) {
            if (err.code == '23505') {  // constraint violation
                console.log('Warning deposit constraint violation for (', userId, ',', offerId, ')');
                return callback(null);
            }
            console.log('[INTERNAL_ERROR] could not complete deposit: (', userId, ',', offerId, ') got err: ', err);
            return callback(err);
        }

        callback();
    });
}

exports.withdrawalCompleted = function(offerId, userId, value, callback) {
    getClient(function(client, callback) {
        async.parallel([
            function(callback) {
                // mark as done
                client.query("UPDATE offers SET status = 'completed' WHERE id = $1 AND (status = 'pending' OR status = 'requested')", [offerId], function (err, res) {
                    if (err) return callback(err);

                    if (res.rowCount !== 1) return callback('ALREADY_PROCESSED');

                    callback();
                });
            },
            function(callback) {
                // add the funding
                client.query("INSERT INTO fundings(offer_id, user_id, amount, description) VALUES($1, $2, $3, 'Skin withdrawal')", [offerId, userId, -value], callback);
            }// ,
            // function(callback) {
            //     // remove items
            //     client.query('DELETE FROM items WHERE withdrawal_id = $1', [offerId], callback);
            // }
        ], callback);
    }, function(err) {
        if (err) {
            if (err.code == '23505') {  // constraint violation
                console.log('Warning withdrawal constraint violation for (', userId, ',', offerId, ')');
                return callback(null);
            }
            console.log('[INTERNAL_ERROR] could not complete withdrawal: (', userId, ',', offerId, ') got err: ', err);
            return callback(err);
        }

        callback();
    });
}

exports.addDeposit = function(offerId, userId, items, value, securityCode, callback) {
    getClient(function(client, callback) {
        async.parallel([
            function(callback) {
                // add the offer
                var sql = "INSERT INTO offers (id, user_id, value, security_code, type) VALUES($1, $2, $3, $4, 'deposit');";
                client.query(sql, [offerId, userId, value, securityCode], callback);
            },
            function(callback) {
                // add the items
                // todo: make a nice query with unnests and shit for this
                var query_values = [];
                var query_data = [];
                for (var i = 0; i < items.length; i++) {
                    query_values.push('($' + ((i * 3) + 1) + ', $' + ((i * 3) + 2) + ', $' + ((i * 3) + 3) + ')');
                    query_data.push(items[i].id, items[i].category.steam_market_hash, offerId);
                }

                var sql = 'INSERT INTO items(id, name, deposit_id) VALUES ' + query_values.join(', ');
                client.query(sql, query_data, callback);
            }
        ], callback);
    }, function (err) {
        if (err) {
            if (err.code == '23505') {  // constraint violation
                console.log('Warning deposit constraint violation for (', userId, ',', offerId, ')');
                return callback(null);
            }
            console.log('[INTERNAL_ERROR] could not add deposit: (', userId, ',', offerId, ') got err: ', err);
            return callback(err);
        }

        callback();
    });
}

exports.addWithdrawal = function(offerId, userId, itemIds, value, securityCode, callback) {
    getClient(function(client, callback) {
        async.series([
            function(callback) {
                // add the offer
                var sql = "INSERT INTO offers (id, user_id, value, security_code, type) VALUES($1, $2, $3, $4, 'withdrawal')";
                client.query(sql, [offerId, userId, value, securityCode], callback);
            },
            function(callback) {
                // mark items as withdrawn
                client.query('UPDATE items SET withdrawal_id = $1 WHERE id = ANY($2)', [offerId, itemIds], callback);
            }
        ], callback);
    }, function(err) {
        if (err) {
            if (err.code == '23505') {  // constraint violation
                console.log('Warning withdraw constraint violation for (', userId, ',', offerId, ')');
                return callback(null);
            }
            console.log('[INTERNAL_ERROR] could not add withdrawal: (', userId, ',', offerId, ') got err: ', err);
            return callback(err);
        }

        callback();
    });
}

exports.setOfferPending = function(offerId, tradeOfferId, callback) {
    async.parallel([
        function (callback) {
            // set tradeofferid
            query('UPDATE offers SET tradeoffer_id = $1 WHERE id = $2', [tradeOfferId, offerId], callback);
        },
        function (callback) {
            // set status to pending
            query("UPDATE offers SET status = 'pending' WHERE id = $1 AND status = 'requested'", [offerId], callback);
        }
    ], callback);
}

exports.getAvailableItems = function(callback) {
    query("SELECT * FROM items WHERE withdrawal_id IS NULL AND received = true", function(err, data) {
        if (err)
            return callback(err);
        callback(null, data.rows);
    });
}

exports.getItems = function(itemIds, callback) {
    var sql = "SELECT * FROM items WHERE id = ANY($1)";
    query(sql, [itemIds], function(err, data) {
        if (err)
            return callback(err);
        callback(null, data.rows);
    });
}

exports.getItemsByOffer = function(offerId, callback) {
    var sql = "SELECT * FROM items WHERE deposit_id = $1 OR withdrawal_id = $1";
    query(sql, [offerId], function(err, data) {
        if (err)
            return callback(err);
        callback(null, data.rows);
    });
}

exports.getLeaderBoard = function(byDb, order, callback) {
    var sql = 'SELECT * FROM leaderboard ORDER BY ' + byDb + ' ' + order + ' LIMIT 100';
    query(sql, function(err, data) {
        if (err)
            return callback(err);
        callback(null, data.rows);
    });
};

exports.addChatMessage = function(userId, created, message, channelName, isBot, callback) {
    var sql = 'INSERT INTO chat_messages (user_id, created, message, channel, is_bot) values($1, $2, $3, $4, $5)';
    query(sql, [userId, created, message, channelName, isBot], function(err, res) {
        if(err)
            return callback(err);

        assert(res.rowCount === 1);

        callback(null);
    });
};

exports.getChatTable = function(limit, channelName, callback) {
    assert(typeof limit === 'number');
    var sql = "SELECT chat_messages.created AS date, 'say' AS type, users.steamid, users.nickname, users.userclass AS role, chat_messages.message, is_bot AS bot " +
        "FROM chat_messages JOIN users ON users.id = chat_messages.user_id WHERE channel = $1 ORDER BY chat_messages.id DESC LIMIT $2";
    query(sql, [channelName, limit], function(err, data) {
        if(err)
            return callback(err);
        callback(null, data.rows);
    });
};

//Get the history of the chat of all channels except the mods channel
exports.getAllChatTable = function(limit, callback) {
    assert(typeof limit === 'number');
    var sql = m(function(){/*
     SELECT chat_messages.created AS date, 'say' AS type, users.steamid, users.nickname, users.userclass AS role, chat_messages.message, is_bot AS bot, chat_messages.channel AS "channelName"
     FROM chat_messages JOIN users ON users.id = chat_messages.user_id WHERE channel <> 'moderators'  ORDER BY chat_messages.id DESC LIMIT $1
    */});
    query(sql, [limit], function(err, data) {
        if(err)
            return callback(err);
        callback(null, data.rows);
    });
};

exports.getSiteStats = function(callback) {

    function as(name, callback) {
        return function(err, results) {
            if (err)
                return callback(err);

            assert(results.rows.length === 1);
            callback(null, [name, results.rows[0]]);
        }
    }

    var tasks = [
        function(callback) {
            query('SELECT COUNT(*) FROM users', as('users', callback));
        },
        function (callback) {
            query('SELECT COUNT(*) FROM games', as('games', callback));
        },
        function(callback) {
            query('SELECT COALESCE(SUM(fundings.amount), 0)::bigint sum FROM fundings WHERE amount < 0', as('withdrawals', callback));
        },
        function(callback) {
            query('SELECT COALESCE(SUM(fundings.amount), 0)::bigint sum FROM fundings WHERE amount > 0', as('deposits', callback));
        },
        function(callback) {
            query('SELECT ' +
                'COUNT(*) count, ' +
                'SUM(plays.bet)::bigint total_bet, ' +
                'SUM(plays.cash_out)::bigint cashed_out, ' +
                'SUM(plays.bonus)::bigint bonused ' +
                'FROM plays', as('plays', callback));
        }
    ];

    async.series(tasks, function(err, results) {
       if (err) return callback(err);

       var data = {};

        results.forEach(function(entry) {
           data[entry[0]] = entry[1];
        });

        callback(null, data);
    });

};

exports.getDepositsAmount = function(userId, callback) {
    assert(userId);
    query('SELECT SUM(f.amount) FROM fundings f WHERE user_id = $1 AND amount >= 0', [userId], function(err, result) {
        if (err) return callback(err);
        callback(null, result.rows[0]);
    });
};

exports.getWithdrawalsAmount = function(userId, callback) {
    assert(userId);
    query('SELECT SUM(f.amount) FROM fundings f WHERE user_id = $1 AND amount < 0', [userId], function(err, result) {
        if (err) return callback(err);

        callback(null, result.rows[0]);
    });
};

exports.hasPendingOffers = function(userId, callback) {
    query("SELECT id FROM offers WHERE user_id = $1 AND (status = 'pending' OR status = 'requested') LIMIT 1", [userId], function(err, result) {
        if (err) return callback(err);

        callback(null, result.rows.length !== 0);
    });
}

exports.getInventory = function(userId, callback) {
    query("SELECT * FROM inventories WHERE user_id = $1", [userId], function(err, result) {
        if (err) return callback(err);

        callback(null, result.rows[0]);
    });
}

exports.updateInventory = function(userId, error, items, newUser, callback) {
    var sql;
    if (newUser)
        sql = 'INSERT INTO inventories(error, items, user_id) VALUES($1, $2, $3)';
    else
        sql = 'UPDATE inventories SET error = $1, items = $2, retrieved = NOW(), requests = requests + 1 WHERE user_id = $3';

    query(sql, [error, JSON.stringify(items), userId], function(err, data) {
        if (err) return callback(err);

        callback(null);
    });
}

exports.getServer = function(serverId, callback) {
    query("SELECT * FROM servers WHERE id = $1", [serverId], function(err, result) {
        if (err) return callback(err);

        callback(null, result.rows[0]);
    });
}

exports.getServers = function(callback) {
    query("SELECT * FROM servers ORDER BY id ASC", function(err, result) {
        if (err) return callback(err);

        callback(null, result.rows);
    });
}

exports.addConnection = function(userId, service, identifier, token, profile, callback) {
    var sql = 'INSERT INTO connections (user_id, service, identifier, token, profile) VALUES($1, $2, $3, $4, $5)';
    query(sql, [userId, service, identifier, token, JSON.stringify(profile)], function(err, data) {
        if (err) return callback(err);

        callback(null);
    });
}

exports.setReferralCode = function(userId, code, callback) {
    query('UPDATE users SET referral_code = $1 WHERE id = $2', [code, userId], function(err, res) {
        if (err) {
            if (err.code === '23505')
                return callback('CODE_TAKEN');

            return callback(err);
        }

        callback();
    });
}

exports.useReferralCode = function(userId, userVerified, code, callback) {
    // get the code owner
    query('SELECT id, nickname FROM users WHERE referral_code = $1', [code], function(err, res) {
        if (err) return callback(err);

        if (res.rows.length !== 1) return callback('INCORRECT_CODE');

        var referrer = res.rows[0];
        if (userId == referrer.id) return callback('OWN_CODE');

        getClient(function(client, callback) {
            async.parallel([
                function(callback) {
                    // set the referrer (also check if the person has already been referred) and give the bonus
                    client.query("UPDATE users SET referrer_id = $1, balance = balance + $2, referred_time = NOW() WHERE id = $3 AND referrer_id IS NULL RETURNING id", [referrer.id, config.REFERRAL_BONUS, userId], function (err, res) {
                        if (err) return callback(err);

                        if (res.rowCount !== 1) return callback('CODE_ALREADY_USED');

                        callback();
                    });
                },
                function(callback) {
                    if (!userVerified) return callback();
                    client.query('UPDATE users SET referred = referred + 1 WHERE id = $1', [referrer.id], callback);
                }
            ], callback);
        }, function(err) {
            if (err) {
                console.log('[INTERNAL_ERROR] could not use referral code: (', userId, ', ', code ,') got err: ', err);
                return callback(err);
            }

            callback(null, referrer, config.REFERRAL_BONUS);
        });
    });
}

exports.getReferred = function(userId, callback) {
    query('SELECT steamid, nickname, referral_profit, referred_time FROM users WHERE referrer_id = $1 AND verified = true ORDER BY referral_profit DESC', [userId], function (err, res) {
        if (err) return callback(err);

        callback(null, res.rows);
    });
}

exports.getConnections = function(userId, callback) {
    query('SELECT * FROM connections WHERE user_id = $1', [userId], function (err, res) {
        if (err) return callback(err);

        callback(null, res.rows);
    });
}

exports.setUserClass = function(userId, userclass, callback) {
    query('UPDATE users SET userclass = $1 WHERE id = $2', [userclass, userId], function (err, res) {
        if (err) return callback();

        // if (res.rowCount !== -1) return callback('USER_NOT_FOUND');

        callback(null);
    })
}