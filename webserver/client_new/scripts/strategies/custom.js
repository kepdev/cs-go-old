define(function(){
    return "// This strategy editor is in BETA mode, please\n\
// exercise extreme caution and use exclusively at\n\
// your own risk. No bets can or will be refunded in\n\
// case of errors.\n\
\n\
// Please note the strategy editor executes arbitrary\n\
// javascript without a sandbox and as such, only use\n\
// strategies from trusted sources, as they can be\n\
// backdoored to lose all your money or have\n\
// intentional exploitable weaknesses etc.\n\
\n\
//Engine events: \n\
\n\
engine.on('game_starting', function(info) {\n\
    console.log('Game Starting in ' + info.time_till_start);\n\
});\n\
\n\
engine.on('game_started', function(data) {\n\
    console.log('Game Started', data);\n\
});\n\
\n\
engine.on('game_crash', function(data) {\n\
    console.log('Game crashed at ', data.game_crash);\n\
});\n\
\n\
engine.on('player_bet', function(data) {\n\
    console.log('The player ', data.steamid, data.nickname, ' placed a bet. This player could be me :o.')\n\
});\n\
\n\
engine.on('cashed_out', function(resp) {\n\
    console.log('The player ', resp.steamid, resp.nickname, ' cashed out. This could be me.');\n\
});\n\
\n\
engine.on('msg', function(data) {\n\
    console.log('Chat message!...');\n\
});\n\
\n\
engine.on('connect', function() {\n\
    console.log('Client connected, this wont happen when you run the script');\n\
});\n\
\n\
engine.on('disconnect', function() {\n\
    console.log('Client disconnected');\n\
});\n\
\n\
\n\
//Getters:\n\
console.log('Balance: ' + engine.getBalance());\n\
console.log('The current payout is: ' + engine.getCurrentPayout());\n\
console.log('My steamid is: ', engine.getSteamID());\n\
console.log('The max current bet is: ', engine.getMaxBet()/100, ' coins');\n\
console.log('The current maxWin is: ', engine.getMaxWin()/100, ' coins');\n\
// engine.getEngine() for raw engine \n\
\n\
\n\
//Helpers:\n\
console.log('Was the last game played? ', engine.lastGamePlayed()?'Yes':'No');\n\
console.log('Last game status: ', engine.lastGamePlay());\n\
\n\
\n\
//Actions:\n\
//Do this between the 'game_starting' and 'game_started' events\n\
//engine.placeBet(bet in coins * 100, auto cash out in percent, auto-play);\n\
\n\
//engine.cashOut(); //Do this when playing\n\
//engine.stop(); //Stops the strategy\n\
//engine.chat('Hello Spam');\n";
});
