define([
    'react',
    'game-logic/GameEngineStore',
    'stores/GameSettingsStore',
    'actions/GameSettingsActions',
    'game-logic/clib',
    'screenfull'
], function(
    React,
    Engine,
    GameSettingsStore,
    GameSettingsActions,
    Clib,
    Screenfull //Attached to window.screenfull
) {
    var D = React.DOM;

    function getState() {
        return {
            balance: Clib.formatSatoshis(Engine.balance),
            steamid: Engine.steamid,
            nickname: Engine.nickname,
            theme: GameSettingsStore.getCurrentTheme()//black || white
        };
    }

    return React.createClass({
        displayName: 'TopBar',
        mixins: [React.addons.PureRenderMixin],

        propTypes: {
            isMobileOrSmall: React.PropTypes.bool.isRequired
        },

        getInitialState: function() {
            var state = getState();
            state.fullScreen = false;
            return state;
        },

        componentDidMount: function() {
            Engine.on({
                joined: this._onChange,
                game_started: this._onChange,
                game_crash: this._onChange,
                cashed_out: this._onChange
            });
            GameSettingsStore.on('all', this._onChange);
        },

        componentWillUnmount: function() {
            Engine.off({
                joined: this._onChange,
                game_started: this._onChange,
                game_crash: this._onChange,
                cashed_out: this._onChange
            });
            GameSettingsStore.off('all', this._onChange);
        },

        _onChange: function() {
            if(this.isMounted())
                this.setState(getState());
        },

        _toggleTheme: function() {
            GameSettingsActions.toggleTheme();
        },

        _toggleFullScreen: function() {
            window.screenfull.toggle();
            this.setState({ fullScreen: !this.state.fullScreen });
        },

        render: function() {

            var userLogin;
            if (this.state.steamid) {
                userLogin = D.div({ className: 'user-login' },
                    D.div({ className: 'balance-bits' },
                        D.span(null, 'Coins: '),
                        D.span({ className: 'balance', style: {paddingRight: '0.2rem'} }, this.state.balance ),
                        ' ',
                        this.props.isMobileOrSmall ? '' : D.a({ href: '/deposit', style: {padding: '0 0.4rem'} }, 'Deposit'),
                        ' ',
                        this.props.isMobileOrSmall ? '' : D.a({ href: '/withdraw', style: {padding: '0 0.4rem'} }, 'Withdraw')
                    ),
                    D.div({ className: 'username' },
                        D.a({ href: '/account'}, this.state.nickname
                    ))
                );
            } else {
                userLogin = D.div({ className: 'user-login' },
                    D.div({ className: 'login' },
                        D.a({ href: '/login', style: {lineHeight: '2.5rem'}}, D.img({src: '/img/sign-in.png', style: {height: '1.2rem', lineHeight: '2rem'}}) )
                    )
                );
            }

            return D.div({ id: 'top-bar' },
                D.div({ className: 'title' },
                    D.a({ href: '/' },
                        D.h1(null, this.props.isMobileOrSmall ? 
                            D.img({src: '/img/logo.png', style: {height: '40px'}})
                        :
                            D.img({src: '/img/logo_full.png', style: {height: '40px'}})
                        )
                    )
                ),
                userLogin,
                // D.div({ className: 'toggle-view noselect' + ((this.state.theme === 'white')? ' black' : ' white'), onClick: this._toggleTheme },
                //     D.a(null,
                //         (this.state.theme === 'white')? 'Night mode' : 'Light mode'
                //     )
                // ),
                D.div({ className: 'full-screen noselect', onClick: this._toggleFullScreen },
                    this.state.fullScreen? D.i({ className: 'fa fa-compress' }) : D.i({ className: 'fa fa-expand' })
                )
            )
        }
    });
});