define([
    'dispatcher/AppDispatcher',
    'constants/AppConstants'
], function(
    AppDispatcher,
    AppConstants
){

    var ChatActions = {

        say: function(msg){
            AppDispatcher.handleViewAction({
                actionType: AppConstants.ActionTypes.SAY_CHAT,
                msg: msg
            });
        },

        ignoreUser: function(steamid) {
            AppDispatcher.handleViewAction({
                actionType: AppConstants.ActionTypes.IGNORE_USER,
                steamid: steamid
            });
        },

        approveUser: function(steamid) {
            AppDispatcher.handleViewAction({
                actionType: AppConstants.ActionTypes.APPROVE_USER,
                steamid: steamid
            });
        },

        showClientMessage: function(message) {
            AppDispatcher.handleViewAction({
                actionType: AppConstants.ActionTypes.CLIENT_MESSAGE,
                message: message
            });
        },

        listMutedUsers: function(ignoredClientList) {
            AppDispatcher.handleViewAction({
                actionType: AppConstants.ActionTypes.LIST_MUTED_USERS,
                ignoredClientList: ignoredClientList
            });
        },

        selectChannel: function(channelName) {
            AppDispatcher.handleViewAction({
                actionType: AppConstants.ActionTypes.JOIN_CHANNEL,
                channelName: channelName
            });
        },

        closeCurrentChannel: function() {
            AppDispatcher.handleViewAction({
                actionType: AppConstants.ActionTypes.CLOSE_CURRENT_CHANNEL
            });
        }

    };

    return ChatActions;
});