function format (n) {
  return n.toFixed(0).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}

function transform (str) {
  return str.replace('™', '').replace(' | ', ' ').replace('-', '').replace('(', '').replace(')', '').toLowerCase();
}

Inventory = function ($parent, $paginationButtons) {
  this.allItems = [];
  this.items = [];

  this.warning;

  this.$parent = $parent;

  this.page = 1;
  this.perPage = 4 * 5; // 4 rows;

  this.selectedItems = [];
  this.maxItems = 10;

  this.$paginationButtons = $paginationButtons;
  this.$paginationButtons.hide();
  var self = this;
  $paginationButtons.each(function (i, button) {
    $(button).on('mousedown', function (e) {
      if (e.which !== 1) return;
      e.preventDefault();

      var offset = parseInt($(this).data('page'));

      self.setPage(self.page + offset);
      self.render();
    });
  });
}

Inventory.prototype.retrieve = function (url, refresh, callback) {
  var self = this;
  $.ajax({
    url: url,
    data: {
      refresh: refresh === true ? true : undefined
    },
    dataType: 'json',
    success: function (data) {
      if (data.error) self.warning = data.error;
      self.refreshed = data.refreshed;
      self.retrieved = data.retrieved;
      self.retrieved_str = data.retrieved_str;
      self.csrfToken = data.csrfToken;

      self.selectedItems = [];

      // format the result
      var result = [];
      for (name in data.items) {
        var item = data.items[name];

        for (var i = 0; i < item.itemids.length; i++) {
          result.push({
            id: item.itemids[i],
            name: name,
            transformed_name: transform(name),
            icon: item.icon,
            price: item.price,
            junk: item.junk
          });
        }
      }

      // sort by prices from high to low
      result.sort(function (b, a) {
        if (a.price > b.price) return 1;
        else if (a.price < b.price) return -1;

        if (a.name < b.name) return -1
        else if (a.name > b.name) return 1;
        else return 0;
      });

      self.allItems = result;
      self.items = result.slice(); // copy array

      self.$paginationButtons.show();

      callback();
    },
    error: function (xhr, errorType, error) {
      callback(error);
      console.log(error);
    }
  });
}

Inventory.prototype.sort = function (by, order) {
  this.items.sort(function (a, b) {
    var aVal = a[by];
    var bVal = b[by];

    // if sorting by name, remove stupid stuff first
    if (by == 'name') {
      aVal = aVal.replace('★', '').replace('StatTrak™', '').replace('Souvenir', '').trim();
      bVal = bVal.replace('★', '').replace('StatTrak™', '').replace('Souvenir', '').trim();
    }

    // actual sorting
    if (aVal > bVal) return order;
    else if (aVal < bVal) return -order;
 
    // if sorting by price, sort by name where price is the same
    if (by == 'price') {
      if (a.name < b.name) return -order;
      else if (a.name > b.name) return order;
      else return 0;
    }
  });
}

Inventory.prototype.filter = function (filter) {
  var filter = transform(filter);
  this.lastFilter = filter;

  // empty filter - all item match
  if (!filter || filter == "") {
    this.items = this.allItems.slice();
    return;
  }

  // if the old filter is a part of the new filter (a letter was added, etc.) just filter the previous results 
  var t = this.allItems;
  if (filter.indexOf(this.lastFilter) === -1) 
    t = this.items;

  // actual filtering
  var results = [];
  for (var i = 0; i < t.length; i++) {
    var item = t[i];
    if (item.transformed_name.indexOf(filter) !== -1) {
      results.push(item);
    }
  }

  this.items = results;
}

Inventory.prototype.renderItem = function (item) {
  var $parent = $('<span/>');
  $parent.addClass('item');
  $parent.css('backgroundImage', 'url("' + item.icon + '/116x116f")');

  if (item.selected)
    $parent.addClass('item-selected');
  if (item.junk)
    $parent.addClass('item-junk');

  var $name = $('<span/>');
  $name.addClass('item-name');
  $name.text(item.name);
  $name.appendTo($parent);

  var $price = $('<span/>');
  $price.addClass('item-price');
  if (!item.junk)
    $price.text(format(item.price));
  else
    $price.text(item.junk);
  $price.appendTo($parent);

  var self = this;
  if (!item.junk)
    $parent.on('mousedown', function (e) {
      if (e.which !== 1) return;
      e.preventDefault();

      if (self.selectItem(item))
        $parent.toggleClass('item-selected');
    });

  return $parent;
}

Inventory.prototype.selectItem = function (item) {
  if (!item.selected && this.selectedItems.length >= this.maxItems) return false;

  item.selected = !item.selected;

  if (item.selected)
    this.selectedItems.push(item);
  else
    this.selectedItems.splice(this.selectedItems.indexOf(item), 1);

  return this.itemSelected(item);
}

Inventory.prototype.render = function () {
  this.$parent.empty();

  var from = (this.page - 1) * this.perPage;
  var to = from + this.perPage;

  for (var i = from; i < to; i++) {
    var item = this.items[i];
    if (!item) break;

    this.$parent.append(this.renderItem(item));
  }
}

Inventory.prototype.setPage = function (page) {
  page = Math.min(page, Math.floor(this.items.length / this.perPage) + 1);
  page = Math.max(page, 1);

  this.page = page;
}

Inventory.prototype.getItems = function () {
  var arr = [];
  for (var i = 0; i < this.selectedItems.length; i++) {
    arr.push(this.selectedItems[i].id);
  }

  return arr;
}

Zepto(function ($) {
  var action = location.pathname == "/deposit" ? "deposit" : "withdraw";

  var inv = new Inventory($('#items'), $('.button[data-page]'));
  inv.maxItems = ITEM_LIMIT;

  // on item selected
  var sum = 0;
  inv.itemSelected = function (item) {
    if (item.selected)
      sum += item.price;
    else
      sum -= item.price;

    if (action == "withdraw" && sum > BALANCE) {
      alert('You can not afford this item.');
      sum -= item.price;

      return false;
    }

    return true;
  }

  function load(refresh, callback) {
    inv.retrieve(action + '/items.json', refresh, function () {
      // errored? display error
      if (inv.warning) {
        var $warning = $('<div class="warning"/>')
        $warning.text(inv.warning);

        $('.page-content').before($warning);
      } else if (action == 'deposit') {
        var $success = $('<div class="success"/>');
        $success.text(inv.refreshed ? 'Inventory refreshed' : 'Loaded cached inventory (from ' + inv.retrieved_str + ').');

        $('.page-content').before($success);
      }

      // render the inventory
      inv.setPage(1);
      inv.render();

      // load saved sort mode
      if (!refresh && localStorage.sortMode)
          $('#sort').val(localStorage.sortMode).change();

      if (callback) callback();
    });
  }
  $.pgwModal({
    content: 'Loading inventory...',
    closable: false
  });

  load(false, function () {
    $.pgwModal('close');
  });

  // refresh button
  if (action == 'deposit') {
    $('#refresh').on('click', function () {
      $('.success, .warning').remove();
      $('#items').empty();

      $.pgwModal({
        content: 'Refreshing inventory...',
        closable: false
      });
      load(true, function () {
        $.pgwModal('close');
      });
    });
  }

  // filtering
  $('#filter').on('keyup', function () {
    inv.filter($(this).val());
    inv.setPage(1);
    inv.render();
  });

  // sorting
  $('#sort').change(function () {
    var val = $(this).val();
    var sort = val.split(':');
    inv.sort(sort[0], parseInt(sort[1]));
    inv.setPage(1);
    inv.render();

    // remember
    localStorage.sortMode = val;
  });

  // sending
  $('#' + action).click(function () {
    // display the thing
    $.pgwModal({
      closable: false,
      content: 'Please wait while your trade offer is being sent...<br><small>(this might take up to 5 minutes)</small>'
    });

    $.ajax({
      type: 'post',
      url: '/' + action + '.json',
      dataType: 'json',
      data: {
        items: inv.getItems(),
        _csrf: inv.csrfToken
      },
      success: function (data) {
        if (data.error) {
          $.pgwModal({
            content: data.error
          });

          return;
        }

        $.pgwModal({
          content: 'Trade offer has been sent! Your security code is: ' + data.code + '<br><br>' +
            '<a href="' + data.url + '" class="button" target="_blank">Open in browser</a>'
        });
      },
      error: function (error) {
        console.log(error);
        $.pgwModal({
          content: 'Could not request trade offer.'
        });
      }
    })
  })

});