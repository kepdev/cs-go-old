function format (n) {
    return n.toFixed(0).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}

Zepto(function ($) {
    var max_items = ITEM_LIMIT;
    $('.item-checkbox').change(function() {
        var checked = $('.item-checkbox:checked');

        if (checked.length > 0) {
            $('.selected-items-info').show();

            if (checked.length > max_items) {
                this.checked = false;
                alert('You can select up to ' + max_items + ' items.');
                return;
            }

            var total = 0;
            checked.each(function(i, item) {
                total += parseInt($(item).data('price'));
            });

            $('.selected-items-info .cost').html(format(total));
        } else {
            $('.selected-items-info').hide();
        }
    });

    /* filtering */
    $('#sort').change(function() {
        var mode = this.value.split(':');

        localStorage.sortMode = this.value;

        tinysort($('.item'), {data: mode[0], order: mode[1]})
    });

    if (localStorage.sortMode)
        $('#sort').val(localStorage.sortMode).change();

    $('#filter').on('keyup', function() {
        var value = this.value.replace('-', ' ');
        $('.item').each(function(i, item) {
            var $item = $(item);
            var name = $item.data('name').replace('|', '').replace(/\s+/, '').replace('-', ' ').toLowerCase();

            if (name.indexOf(value) === -1) {
                $item.hide();
            } else {
                $item.show();
            }
        })
    });
    $('#hidejunk').change(function() {
        var checked = this.checked;
        localStorage.hideJunk = checked;

        $('.item').each(function(i, item) {
            var $item = $(item);
            if ($item.data('price') == 0) {
                if (checked) {
                    $item.hide();
                } else {
                    $item.show();
                }
            }
        });
    });

    if (localStorage.hideJunk)
        $('#hidejunk').prop('checked', localStorage.hideJunk == 'true').change();

    $('form[action="/deposit-request"]').submit(function () {
        $('input[type="submit"]').attr("disabled", true);
    });
});