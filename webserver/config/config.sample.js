/**
 * For development you can set the variables by creating a .env file on the root
 */
var fs = require('fs');
var production = process.env.NODE_ENV === 'production';

var prodConfig;
if(production) {
  prodConfig = JSON.parse(fs.readFileSync(__dirname + '/build-config.json'));
  console.log('Build config loaded: ', prodConfig);
}

module.exports = {
  "PRODUCTION": production,
  "DATABASE_URL": process.env.DATABASE_URL || "postgres://csgocrash:ass123@localhost/csgocrashdb?ssl=true",
  "CONTACT_EMAIL": process.env.CONTACT_EMAIL || "support@csgocrash.com",
  "SITE_URL": process.env.SITE_URL || "https://csgocrash.com",
  "LOGIN_URL": process.env.LOGIN_URL || "http://csgocrash-login.com",
  "BANKROLL_OFFSET": parseInt(process.env.BANKROLL_OFFSET) || 0,
  "PORT":  process.env.PORT || 3841,
  "BUILD": prodConfig,
  "SOCKET_IO_CONFIG": {
    allowUpgrades: false // Do not upgrade transport to WS
  },

  "STEAM_API_KEY": '',

  "ITEMPLATFORM_ACCOUNT_ID": process.env.ITEMPLATFORM_ACCOUNT_ID || '',
  "ITEMPLATFORM_TOKEN_USERNAME": process.env.ITEMPLATFORM_TOKEN_USERNAME || '',
  "ITEMPLATFORM_TOKEN_PASSWORD": process.env.ITEMPLATFORM_TOKEN_PASSWORD || '',

  "STEAMANALYST_KEY": process.env.STEAMANALYST_KEY || '',

  "MIN_ITEM_VALUE": 1000,

  "DISABLE_BOTS": false
};
