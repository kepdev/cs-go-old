/**
 * For development you can set the variables by creating a .env file on the root
 */
var fs = require('fs');
var production = process.env.NODE_ENV === 'test';
console.log("ENV: " + process.env.NODE_ENV);
var prodConfig;
if(production) {
  prodConfig = JSON.parse(fs.readFileSync(__dirname + '/build-config.json'));
  console.log('Build config loaded: ', prodConfig);
}

module.exports = {
  "PRODUCTION": production,
  "DATABASE_URL": process.env.DATABASE_URL || "postgres://postgres:postgres@postgres/postgres?ssl=false",
  "CONTACT_EMAIL": process.env.CONTACT_EMAIL || "support@csgocrash.com",
  "SITE_URL": process.env.SITE_URL || "http://localhost:3841",
  "LOGIN_URL": process.env.LOGIN_URL || "http://localhost:3841",
  "BANKROLL_OFFSET": parseInt(process.env.BANKROLL_OFFSET) || 0,
  "PORT":  process.env.PORT || 3841,
  "BUILD": prodConfig,
  "SOCKET_IO_CONFIG": {
    allowUpgrades: false // Do not upgrade transport to WS
  },

  "STEAM_API_KEY": "533308DE2BD4FEE78CABE2DF017CC4AE",

  "ITEMPLATFORM_ACCOUNT_ID": process.env.ITEMPLATFORM_ACCOUNT_ID || '',
  "ITEMPLATFORM_TOKEN_USERNAME": process.env.ITEMPLATFORM_TOKEN_USERNAME || '',
  "ITEMPLATFORM_TOKEN_PASSWORD": process.env.ITEMPLATFORM_TOKEN_PASSWORD || '',

  "STEAMANALYST_KEY": process.env.STEAMANALYST_KEY || '',

  "MIN_ITEM_VALUE": 1000,

  "DISABLE_BOTS": false
};
